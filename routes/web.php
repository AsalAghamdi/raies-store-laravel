<?php

Route::get('/', function () {
    return view('welcome');
});

// web pages
Route::view('index','web-pages/index');
Route::view('about','web-pages/about');
Route::view('services','web-pages/services');
Route::view('service','web-pages/service');
Route::view('projects','web-pages/projects');
Route::view('project','web-pages/project');
Route::view('offers','web-pages/offers');
Route::view('offer','web-pages/offer');
Route::view('jobs','web-pages/jobs');
Route::view('job','web-pages/job');
Route::view('job-apply','web-pages/job-apply');
Route::view('blogs','web-pages/blogs');
Route::view('blog','web-pages/blog');
Route::view('clients','web-pages/clients');
Route::view('client','web-pages/client');
Route::view('contact','web-pages/contact');


// admin pages
Route::view('admin-index','admin-pages/index');
Route::view('admin-classes','admin-pages/classes');
Route::view('admin-profile','admin-pages/profile');
Route::view('admin-tables','admin-pages/tables');
Route::view('admin-datatables','admin-pages/datatables');
Route::view('admin-chart','admin-pages/chart');
Route::view('admin-forms','admin-pages/forms');
Route::view('admin-steps-form','admin-pages/steps-form');

// Raies
// -- products
Route::view('admin-products', 'admin-pages/products/show-all');
Route::view('admin-create-product', 'admin-pages/products/create');
Route::view('admin-measurements', 'admin-pages/products/measurements');
Route::view('admin-create-meas', 'admin-pages/products/create-measurements');
Route::view('admin-gift', 'admin-pages/products/gift');
Route::view('admin-show-gift', 'admin-pages/products/show-gift');
Route::view('show-gift-card', 'admin-pages/products/show-gift-card');
Route::view('gift-page', 'admin-pages/products/gift-page');
Route::view('decoration-gift-page', 'admin-pages/products/decoration-gift-page');
Route::view('admin-product-page', 'admin-pages/products/product-page');
Route::view('create-gift-box', 'admin-pages/products/create-gift-box');
Route::view('create-gift-details', 'admin-pages/products/create-gift-details');

// -- orders
Route::view('admin-orders', 'admin-pages/orders/show-all');
Route::view('order-page', 'admin-pages/orders/order-page');
Route::view('special-order-page', 'admin-pages/orders/special-order-page');
Route::view('special-orders', 'admin-pages/orders/show-special-orders');
Route::view('return-orders', 'admin-pages/orders/show-return-orders');
Route::view('return-order-page', 'admin-pages/orders/return-order-page');

// -- coupons
Route::view('admin-coupons', 'admin-pages/discounts and offers/coupons');
Route::view('admin-discount', 'admin-pages/discounts and offers/discount');
Route::view('coupon-page', 'admin-pages/discounts and offers/coupon-page');
Route::view('discount-page', 'admin-pages/discounts and offers/discount-page');
Route::view('banner-offers', 'admin-pages/discounts and offers/banner-offers');
Route::view('banner-page', 'admin-pages/discounts and offers/banner-page');
Route::view('special-coupons', 'admin-pages/discounts and offers/special-coupons');

// -- customers
Route::view('admin-customers', 'admin-pages/customers/show-all');
Route::view('customer-page', 'admin-pages/customers/customer-page');
Route::view('customer-category', 'admin-pages/customers/customer-category');
Route::view('create-category', 'admin-pages/customers/create-category');

// -- shipment
Route::view('tracking', 'admin-pages/shipment/tracking');

// {{url('create-gift-box')}}