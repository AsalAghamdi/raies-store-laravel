@extends('layouts.admin')

@section('title')
عضويات العملاء
@endsection

@section('css')
<style>
    .container {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
    }

    .container .enter-policy {
        width: 80%;
    }

    .container form {
        display: flex;
        flex-wrap: nowrap;
        padding: 0 0 10px 0;
        /* width: 50%; */
    }

    .round {
        border-radius: 5px;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">تتبع الشحنات</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="container">
                <div class="card enter-policy round">
                    <div class="card-header no-border">
                        <p><strong>
                                أدخل رقم البوليصة
                            </strong>
                        </p>
                        <form action="">
                            <input id="form-control-3" class="form-control" type="text" placeholder="رقم البوليصة">
                            <a class="btn btn-primary pull-right" onclick="document.querySelector('table').style.display='table'">تتبع</a>
                            <!-- <button class="btn btn-primary pull-right" type="submit">تتبع</button> -->
                        </form>
                        <table class="table" style="display: none;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الموقع</th>
                                    <th>الحالة</th>
                                    <th>التاريخ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>مكة المكرمة</td>
                                    <td>تم اخذ القطعة</td>
                                    <td>الاربعاء، 2020/4/6، 12:49 م</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>مكة المكرمة</td>
                                    <td>تم اخذ القطعة</td>
                                    <td>الاربعاء، 2020/4/6، 12:49 م</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>مكة المكرمة</td>
                                    <td>تم اخذ القطعة</td>
                                    <td>الاربعاء، 2020/4/6، 12:49 م</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-header no-border">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection