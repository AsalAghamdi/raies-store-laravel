@extends('layouts.admin')

@section('title')
تفاصيل الطلب
@endsection

@section('css')
<style>
    .order-btn {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
    }

    .order-btn button {
        margin: 0 10px;
        margin-top: 40px
    }

    .card-title span {
        color: var(--admin-primary);
    }

    .row {
        display: flex;
        justify-content: center;
    }

    .row-pay {
        display: flex;
        justify-content: space-between;
        margin: 5px 0;
        align-items: center;
    }

    .row-pay div:first-child {
        display: flex;
        align-items: baseline;
    }

    .row-pay div:first-child p {
        margin: 0 10px;
    }

    .row-pay div:first-child p:first-child {
        font-weight: bold;
        font-size: 120%;
        margin: 0;
    }

    .price {
        color: var(--admin-primary);
        font-size: 90%;
    }

    .total-price {
        border-top: 1px solid #eee;
        padding-top: 10px;
    }

    .total-price p {
        font-weight: bold;
    }

    .payment-method {
        border-bottom: 1px solid #eee;
        padding: 5px 0;
        margin: 0;
    }

    .payment-method p {
        margin: 0;
    }

    img.card-payment {
        height: 30px;
        width: auto;
        display: block;
    }

    .shipment-steps {
        display: flex;
        flex-wrap: nowrap;
        justify-content: space-around;
    }

    .shipment-steps .step p {
        margin-top: 10px;
        font-weight: bold;
        font-size: 120%;
    }

    .shipment-steps .step:hover .circle {
        transform: scale(1.2, 1.2);
    }

    .shipment-steps .step .circle {
        background: #212025;
        color: #fff;
        opacity: 0.5;
        transition: all .2s ease-in-out;
    }

    .shipment-steps .step.active .circle {
        background: var(--admin-primary);
        color: #fff;
        opacity: 1;
    }

    .shipment-steps .step.pass .circle {
        background: var(--admin-secondary);
        color: #fff;
        opacity: 1;
    }

    .info {
        width: 100%;
    }

    .row-info p:first-child {
        display: block;
        font-weight: bold;
    }

    .info-name {
        /* border-bottom: 1px solid #eee; */
        /* margin-bottom: 15px; */
    }

    .phone-no {
        direction: ltr;
    }

    .table-bordered>tbody>tr>td {
        border: none;
        border-bottom: 1px solid #eee;
    }

    .table-bordered {
        border: none;
        border-top: 1px solid #eee;
    }

    /* form */
    input {
        width: 100px !important;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <form action="">
            <div class="title-bar">
                <!-- page title -->
                <h1 class="title-bar-title">
                    <span class="d-ib">تفاصيل الطلب</span>
                </h1>
            </div>
            <div class="card card-body p30px">
                <div class=" gutter-xs" style="position: relative;">
                    <div class="col-sm-6">
                        <h2>
                            رقم الطلب: #93720
                        </h2>
                        <p>
                            2020/03/15
                        </p>
                    </div>
                    <div class="col-sm-6 order-btn">
                        <button class="btn btn-primary btn-sm" type="submit"><span class="icon icon-pencil-square"></span>&nbsp;حفظ التعديلات</button>
                        <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp; حذف الطلب</button>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-11">
                    <div class="row" style="flex-wrap: wrap">
                        <div class="col-md-12">
                            <!-- الطلب الخاص -->
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-diamond"></span>&nbsp; الطلب الخاص</strong>
                                    </h4>
                                </div>
                                <div class="card-body" data-toggle="match-height">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">#</th>
                                                <th rowspan="2">صورة المنتج</th>
                                                <th rowspan="2">رقم المنتج</th>
                                                <th rowspan="2">الملاحظات</th>
                                                <th rowspan="2">اللون</th>
                                                <th rowspan="2">المقاس</th>
                                                <th rowspan="2">العدد</th>
                                                <th colspan="2" class="text-center">علبة الاهداء</th>
                                                <th rowspan="2">السعر الاجمالي</th>
                                            </tr>
                                            <tr>
                                                <th>رقم العلبة</th>
                                                <th>السعر</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <textarea name="" id="" rows="2">
                                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص 
                                                    </textarea>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="الاسود">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="8.5">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="1">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="-">
                                                    <!-- <a href="" class="price">عرض المنتج</a> -->
                                                </td>
                                                <td>-</td>
                                                <td>483 SR</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <textarea name="" id="" rows="2">
                                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص 
                                                    </textarea>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="الاسود">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="8.5">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="1">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="-">
                                                    <!-- <a href="" class="price">عرض المنتج</a> -->
                                                </td>
                                                <td>-</td>
                                                <td>483 SR</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <textarea name="" id="" rows="2">
                                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص 
                                                    </textarea>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="الاسود">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="8.5">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="1">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="-">
                                                    <!-- <a href="" class="price">عرض المنتج</a> -->
                                                </td>
                                                <td>-</td>
                                                <td>483 SR</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <!-- المنتجات -->
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-diamond"></span>&nbsp; المنتجات</strong>
                                    </h4>
                                </div>
                                <div class="card-body" data-toggle="match-height">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">#</th>
                                                <th rowspan="2">صورة المنتج</th>
                                                <th rowspan="2">رقم المنتج</th>
                                                <th rowspan="2">العدد</th>
                                                <th colspan="2" class="text-center">علبة الاهداء</th>
                                                <th rowspan="2">السعر الاجمالي</th>
                                            </tr>
                                            <tr>
                                                <th>رقم العلبة</th>
                                                <th>السعر</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <input class="form-control" type="text" value="1">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="-">
                                                    <!-- <a href="" class="price">عرض المنتج</a> -->
                                                </td>
                                                <td>-</td>
                                                <td>483 SR</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <input class="form-control" type="text" value="2">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="37942">
                                                    <a href="" class="price">عرض المنتج</a>
                                                </td>
                                                <td>34 SR</td>
                                                <td>966 SR</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </td>
                                                <td><a href="">11373-3</a></td>
                                                <td>
                                                    <input class="form-control" type="text" value="2">
                                                    <span class="price">× 483 SR</span>
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" value="37942">
                                                    <a href="" class="price">عرض المنتج</a>
                                                </td>
                                                <td>34 SR</td>
                                                <td>966 SR</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- الدفع -->
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-credit-card"></span>&nbsp; الدفع</strong>
                                    </h4>
                                </div>
                                <div class="card-body" data-toggle="match-height">
                                    <div class="row-pay payment-method">
                                        <div>
                                            <p>طريقة الدفع</p>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="icon icon-credit-card-alt"></span></span>
                                            <input class="form-control" type="text" value="بطاقة مدى" aria-label="SAR" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="row-pay">
                                        <div>
                                            <p>السعر</p>
                                            <p class="price">3 قطع</p>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">SAR</span>
                                            <input class="form-control" type="text" value="3483.00" aria-label="SAR" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="row-pay">
                                        <div>
                                            <p>قيمة الضريبة</p>
                                            <p class="price">VAT 5%</p>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">SAR</span>
                                            <input class="form-control" type="text" value="33.00" aria-label="SAR" disabled="disabled">
                                        </div>
                                    </div>
                                    <div class="row-pay">
                                        <div>
                                            <p>قيمة خصم الكوبون</p>
                                            <p class="price">
                                                (اسم الكوبون)
                                                &nbsp;-30%
                                            </p>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">SAR</span>
                                            <input class="form-control" type="text" value="49.00" aria-label="SAR">
                                        </div>
                                    </div>
                                    <div class="row-pay">
                                        <div>
                                            <p>قيمة خصم العرض</p>
                                            <p class="price">
                                                (اسم العرض)
                                                &nbsp;-30%
                                            </p>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">SAR</span>
                                            <input class="form-control" type="text" value="00.00" aria-label="SAR">
                                        </div>
                                    </div>
                                    <div class="row-pay total-price">
                                        <div>
                                            <p>السعر الاجمالي</p>
                                            <!-- <p class="price"></p> -->
                                        </div>
                                        <p>6384 SR</p>
                                    </div>
                                </div>
                            </div>

                            <!-- مراحل الطلب -->
                            <!-- <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-list-ol"></span>&nbsp; مراحل الطلب</strong>
                                    </h4>
                                </div>
                                <div class="card-body" data-toggle="match-height">
                                    <div class="shipment-steps">
                                        <div class="step pass">
                                            <span class="circle sq-40">
                                                <span class="icon icon-credit-card"></span>
                                            </span>
                                            <p>الدفع</p>
                                        </div>
                                        <div class="step active">
                                            <span class="circle sq-40">
                                                <span class="icon icon-shopping-bag"></span>
                                            </span>
                                            <p>تجهيز القطع</p>
                                        </div>
                                        <div class="step">
                                            <span class="circle sq-40">
                                                <span class="icon icon-truck"></span>
                                            </span>
                                            <p>توصيل الطلب</p>
                                        </div>
                                        <div class="step">
                                            <span class="circle sq-40">
                                                <span class="icon icon-check"></span>
                                            </span>
                                            <p>تم استلام الطلب</p>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                        <div class="col-md-4" style="position: relative;">
                            <!-- customer info -->
                            <div class="card info">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-user"></span>&nbsp; العميل</strong>
                                    </h4>
                                </div>
                                <div class="card-body" data-toggle="match-height">

                                    <div class="row-info info-name">
                                        <p>اسم العميل</p>
                                    </div>
                                    <div class="row-info info-name">
                                        <p>عضوية العميل</p>
                                    </div>
                                    <hr>
                                    <div class="row-info">
                                        <p>رقم الجوال:</p>
                                        <p class="phone-no">
                                            +966000000000
                                        </p>
                                    </div>
                                    <div class="row-info">
                                        <div>
                                            <p>البريد الالكتروني:</p>
                                        </div>
                                        <p class="phone-no">
                                            costumer@hotmail.com
                                        </p>
                                    </div>
                                    <div class="row-info">
                                        <div>
                                            <p>العنوان:</p>
                                        </div>
                                        <p>
                                            المملكة العربية السعودية، جدة، حي..
                                        </p>
                                    </div>
                                    <hr>
                                    <div class="row-info">
                                        <div>
                                            <p>عنوان الشحن:</p>
                                        </div>
                                        <textarea id="form-control-8" class="form-control" rows="3">
                                            المملكة العربية السعودية، جدة، حي..
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <!-- <div class="card-actions">
                                        <button type="button" class="card-action card-toggler" title="Collapse"></button>
                                        <button type="button" class="card-action card-reload" title="Reload"></button>
                                        <button type="button" class="card-action card-remove" title="Remove"></button>
                                    </div> -->
                                    <h4 class="card-title">
                                        <strong><span class="icon icon-list-ol"></span>&nbsp;مراحل الطلب</strong>
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-search">
                                        <!-- <div class="card-search-box">
                                            <form action="#">
                                                <div class="form-group">
                                                    <div class="input-with-icon">
                                                        <input class="form-control input-thick pill" type="text" placeholder="Search…">
                                                        <span class="icon icon-search input-icon"></span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div> -->
                                        <div class="card-search-results">
                                            <div class="timeline">
                                                <div class="timeline-item">
                                                    <div class="timeline-segment">
                                                        <div class="timeline-divider"></div>
                                                    </div>
                                                    <div class="timeline-content"></div>
                                                </div>
                                                <div class="timeline-item">
                                                    <div class="timeline-segment">
                                                        <div class="timeline-media bg-primary circle sq-24">
                                                            <div class="icon icon-credit-card"></div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-content">
                                                        <div class="timeline-row" style="display: flex; align-items: center;">
                                                            <h4>مرحلة الدفع</h4>
                                                            &nbsp;
                                                            &nbsp;
                                                            <small>(2020/3/03)</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="timeline-item">
                                                    <div class="timeline-segment">
                                                        <div class="timeline-media bg-primary circle sq-24">
                                                            <div class="icon icon-shopping-bag"></div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-content">
                                                        <div class="timeline-row" style="display: flex; align-items: center;">
                                                            <h4>تجهيز القطع</h4>
                                                            &nbsp;
                                                            &nbsp;
                                                            <small>(جاري العمل)</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="timeline-item">
                                                    <div class="timeline-segment">
                                                        <div class="timeline-media bg-primary circle sq-24">
                                                            <div class="icon icon-truck"></div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-content">
                                                        <div class="timeline-row" style="display: flex; align-items: center;">
                                                            <h4>توصيل الطلب</h4>
                                                            &nbsp;
                                                            &nbsp;
                                                            <small>(لم يبدأ بعد)</small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="timeline-item">
                                                    <div class="timeline-segment">
                                                        <div class="timeline-media bg-primary circle sq-24">
                                                            <div class="icon icon-check"></div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-content">
                                                        <div class="timeline-row" style="display: flex; align-items: center;">
                                                            <h4>استلام الطلب</h4>
                                                            &nbsp;
                                                            &nbsp;
                                                            <small>(لم يبدأ بعد)</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-heading">
                                        <span class="price">يمكن تحديث حالة الطلب من خلال اختيار المرحلة:</span>
                                        <select id="form-control-21" class="custom-select">
                                            <option value="" selected="selected">اختر المرحلة</option>
                                            <option value="c-plus-plus">مرحلة الدفع</option>
                                            <option value="css">تجهيز القطع</option>
                                            <option value="java">توصيل الطلب</option>
                                            <option value="javascript">استلام الطلب</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



















                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection