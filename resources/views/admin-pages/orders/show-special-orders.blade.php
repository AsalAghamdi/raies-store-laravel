@extends('layouts.admin')

@section('title')
الطلبات
@endsection

@section('css')
<style>
    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">الطلبات الخاصة</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>جميع الطلبات الخاصة</strong>
                        </div>
                        <div class="card-body">
                            <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رقم الطلب</th>
                                        <th>التاريخ</th>
                                        <th>رقم العميل</th>
                                        <th>رقم الفاتورة</th>
                                        <th>المبلغ</th>
                                        <th colspan="4" class="text-center">
                                            مراحل الطلب
                                            <div class="steps">
                                                <div>
                                                    الدفع
                                                </div>
                                                <div>
                                                    تجهيز القطعة
                                                </div>
                                                <div>
                                                    الشحن
                                                </div>
                                                <div>
                                                    الاستلام
                                                </div>
                                            </div>
                                        </th>
                                        <th class="text-center">استعراض</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>4794</td>
                                        <td>2020/03/24</td>
                                        <td>74903</td>
                                        <td>33263</td>
                                        <td>2318 SR</td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('special-order-page')}}'">تفاصيل الطلب</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>4794</td>
                                        <td>2020/03/24</td>
                                        <td>74903</td>
                                        <td>33263</td>
                                        <td>2318 SR</td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('special-order-page')}}'">تفاصيل الطلب</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>4794</td>
                                        <td>2020/03/24</td>
                                        <td>74903</td>
                                        <td>33263</td>
                                        <td>2318 SR</td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-check"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <span class="icon icon-clock-o"></span>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('special-order-page')}}'">تفاصيل الطلب</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection