@extends('layouts.admin')

@section('content')
<div class="layout-content">
    <div class="profile">
        <div class="profile-header">
            <div class="profile-cover">
                <div class="profile-container">
                    <div class="profile-card">
                        <div class="profile-avetar">
                            <img class="profile-avetar-img" width="128" height="128" src="{{ asset('assets/admin-theme') }}/images/user.jpg"
                                alt="i-esnaad">
                        </div>
                        <div class="profile-overview">
                            <h1 class="profile-name">محمد محمد</h1>
                            <button class="profile-follow-btn" type="button">متابعة</button>
                            <p>
                            هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق 
                            </p>
                        </div>
                        <div class="profile-info">
                            <ul class="profile-nav">
                                <li>
                                    <a href="#" >
                                        <h3 class="profile-nav-heading">1,143</h3>
                                        <em>
                                            <small>متابعين</small>
                                        </em>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <h3 class="profile-nav-heading">44M</h3>
                                        <em>
                                            <small>معجبين</small>
                                        </em>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <h3 class="profile-nav-heading">125</h3>
                                        <em>
                                            <small>البوست</small>
                                        </em>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- tab content -->
        <div class="profile-body">
            <div class="row justify-content-center">
                
                <div class=" col-md-8 col-md-offset-2 mb10px">
                    <button class="btn btn-danger" type="button">إضافة</button>
                    <button class="btn btn-danger" type="button">إضافة</button>
                    <button class="btn btn-danger" type="button">إضافة</button>
                </div>

                <div class="card col-md-8 col-md-offset-2">
                    <div class="card-header no-border">
                        <button class="btn btn-danger" type="button">إضافة</button>
                    </div>
                </div>

                <!-- طريقة عرض جدول -->
                <div class="card col-md-8 col-md-offset-2">
                    <div class="card-header">
                        <button class="btn btn-danger" type="button">إضافة</button>
                    </div>
                    <div class=" card-body" data-toggle="match-height">
                        <table class="table profile-table">
                            <tr>
                                <td width="20%" class="profile-label fw-700">الاسم</td>
                                <td width="70%" align="right">محمد محمد</td>
                                <td width="10%" align="left">
                                    <a href="#">
                                        <span class="icon icon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="profile-label fw-700">الاسم</td>
                                <td width="70%" align="right">محمد محمد</td>
                                <td width="10%" align="left">
                                    <a href="#">
                                        <span class="icon icon-check"></span>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="profile-label fw-700">الاسم</td>
                                <td width="70%" align="right">محمد عبدالله عمر</td>
                                <td width="10%" align="left">
                                    <a href="#">
                                        <span class="icon icon-check"></span>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <!-- طريقة عرض جدول -->
                <div class="card col-md-8 col-md-offset-2">
                    <div class="card-header">
                        <button class="btn btn-danger" type="button">إضافة</button>
                    </div>
                    <div class=" card-body" data-toggle="match-height">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="#" class="db">
                                    <img class="img-profile" src="{{ asset('assets/admin-theme') }}/images/user.jpg"
                                    alt="i-esnaad">
                                </a>
                                <button class="btn btn-danger btn-block" type="button">إضافة</button>
                                <button class="btn btn-danger btn-block" type="button">إضافة</button>
                                <button class="btn btn-danger btn-block" type="button">إضافة</button>
                            </div>
                            <div class="col-md-10">
                                <table class="table profile-table">
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد محمد</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-pencil"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد محمد</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد عبدالله عمر</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد عبدالله عمر</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد عبدالله عمر</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد عبدالله عمر</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="profile-label fw-700">الاسم</td>
                                        <td width="70%" align="right">محمد عبدالله عمر</td>
                                        <td width="10%" align="left">
                                            <a href="#">
                                                <span class="icon icon-check"></span>
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="clearfix"></div>
            <div class="row">

                <!-- مستطيل كبير -->
                <div class="card col-md-8 col-md-offset-2 mb20px">
                    <div class="card-header">
                        <button class="btn btn-danger" type="button">إضافة</button>
                        <button class="btn btn-danger" type="button">إضافة</button>
                        <button class="btn btn-danger" type="button">إضافة</button>
                        <a href="#" class="pull-right">
                            <span class="icon icon-pencil"></span>
                        </a>
                    </div>
                    <div class=" card-body" data-toggle="match-height">
                        <div class="row mb10px">
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">الاسم</h3>
                                <h4>محمد محمد عبدالله العبدالله</h4>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">الوظيفة</h3>
                                <h4>مدير</h4>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">رقم الجوال</h3>
                                <h4>055555555</h4>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">البريد الإلكتروني</h3>
                                <h4>mail@mail.com</h4>
                            </div>
                        </div>
                        <div class="row mb10px">
                            <div class="col-md-3 text-center profile-label">
                                <a href="#">
                                    <img class="img-profile" src="{{ asset('assets/admin-theme') }}/images/user.jpg"
                                    alt="i-esnaad">
                                </a>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">الاسم</h3>
                                <h4>محمد محمد عبدالله العبدالله</h4>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">الوظيفة</h3>
                                <h4>مدير</h4>
                            </div>
                            <div class="col-md-3 text-center profile-label">
                                <h3 class="fw-700">رقم الجوال</h3>
                                <h4>055555555</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection