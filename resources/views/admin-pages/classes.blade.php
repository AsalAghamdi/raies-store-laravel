@extends('layouts.admin')

@section('content')
<div class="layout-content">
    <div class="layout-content-body">

        <!-- عنوان الصفحة -->
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button">اختصار - انشاء صفحة جديدة</button>
            </div>
            <!-- العنوان -->
            <h1 class="title-bar-title">
                <span class="d-ib">الكلاسات المستخدمة في الثيم</span> - Bootstrap3
            </h1>
        </div>


        <h3>fonts</h3>
        <div class="row gutter-xs">
            <div class="col-xs-1 class-name"><p>fw-700</p></div>
            <div class="col-xs-1 class-name"><p>fw-l</p></div>
            <div class="col-xs-1 class-name"><p>fz-sm </p></div>
            <div class="col-xs-1 class-name"><p>fw-b </p></div>
            <div class="col-xs-1 class-name"><p>bg-gray</p></div>
            <div class="col-xs-1 class-name"><p>text-danger</p></div>
            <div class="col-xs-1 class-name"><p>text-center</p></div>
            <div class="col-xs-1 class-name"><p> sq-64 </p></div>
            <div class="col-xs-1 class-name"><p>link-muted</p></div>
            <div class="col-xs-1 class-name"><p>"d-ib</p></div>
            <div class="col-xs-1 class-name"><p>gutter-xs</p></div>
            <div class="col-xs-1 class-name"><p>title-bar-title</p></div>
            <div class="col-xs-1 class-name"><p>card-values</p></div>
            <div class="col-xs-1 class-name"><p>card-title</p></div>
            <div class="col-xs-1 class-name"><p>card-chart</p></div>
            <div class="col-xs-1 class-name"><p>card-map</p></div>
            <div class="col-xs-1 class-name"><p>card-heading</p></div>
            <div class="col-xs-1 class-name"><p>card-actions</p></div>
            <div class="col-xs-1 class-name"><p>card-action </p></div>
            <div class="col-xs-1 class-name"><p>card-toggler</p></div>
            <div class="col-xs-1 class-name"><p>card-reload</p></div>
            <div class="col-xs-1 class-name"><p>card-remove</p></div>
            <div class="col-xs-1 class-name"><p>card-phrases</p></div>
            <div class="col-xs-1 class-name"><p>card-focused</p></div>
            <div class="col-xs-1 class-name"><p>card-avatar</p></div>
            <div class="col-xs-1 class-name"><p>card-thumbnail</p></div>
            <div class="col-xs-1 class-name"><p>rounded </p></div>
            <div class="col-xs-1 class-name"><p>circle</p></div>
            <div class="col-xs-1 class-name"><p>img-circle</p></div>
            <div class="col-xs-1 class-name"><p>media</p></div>
            <div class="col-xs-1 class-name"><p>media-middle </p></div>
            <div class="col-xs-1 class-name"><p>media-left</p></div>
            <div class="col-xs-1 class-name"><p>media-body</p></div>
            <div class="col-xs-1 class-name"><p>media-heading</p></div>
            <div class="col-xs-1 class-name"><p>media-left</p></div>
            <div class="col-xs-1 class-name"><p>timeline</p></div>
            <div class="col-xs-1 class-name"><p>timeline-content</p></div>
            <div class="col-xs-1 class-name"><p>timeline-item</p></div>
            <div class="col-xs-1 class-name"><p>timeline-segment</p></div>
            <div class="col-xs-1 class-name"><p>timeline-divider</p></div>
            <div class="col-xs-1 class-name"><p>timeline-content</p></div>
            <div class="col-xs-1 class-name"><p>timeline-row</p></div>
            <div class="col-xs-1 class-name"><p>icon-spin</p></div>
            <div class="col-xs-1 class-name"><p>arrow-left </p></div>
            <div class="col-xs-1 class-name"><p>arrow-danger </p></div>
            <div class="col-xs-1 class-name"><p>arrow-lg </p></div>
            <div class="col-xs-1 class-name"><p>pos-r</p></div>
            <div class="col-xs-1 class-name"><p>arrow-outline-danger</p></div>
            <div class="col-xs-1 class-name"><p>badge-above right</p></div>
            <div class="col-xs-1 class-name"><p> icon-fw</p></div>
            <div class="col-xs-2 class-name"><p>spinner spinner-default spinner-sm</p></div>
            <div class="col-xs-2 class-name"><p>spinner spinner-default</p></div>
            <div class="col-xs-1 class-name"><p>spinner-sm</p></div>
            <div class="col-xs-1 class-name"><p>sq-20</p></div>
            <div class="col-xs-1 class-name"><p>sq-32</p></div>
            <div class="col-xs-1 class-name"><p>sq-40</p></div>
            <div class="col-xs-1 class-name"><p>sq-48</p></div>
            <div class="col-xs-1 class-name"><p>sq-80</p></div>
            <div class="col-xs-1 class-name"><p>sq-36 </p></div>
            <div class="col-xs-1 class-name"><p>sq-64</p></div>
            <div class="col-xs-1 class-name"><p>divider</p></div>
            <div class="col-xs-1 class-name"><p>divider-content</p></div>
            <div class="col-xs-1 class-name"><p>divider-vertical</p></div>
            <div class="col-xs-1 class-name"><p>ul.file-list</p></div>
            <div class="col-xs-1 class-name"><p>file</p></div>
            <div class="col-xs-1 class-name"><p>file-link</p></div>
            <div class="col-xs-1 class-name"><p>file-thumbnail</p></div>
            <div class="col-xs-1 class-name"><p>file-info</p></div>
            <div class="col-xs-1 class-name"><p>icon-with-child</p></div>
            <div class="col-xs-1 class-name"><p>icon-child</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card</p></div>
            <div class="col-xs-2 class-name"><p>pricing-card-recommended</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-header</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-body</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-price</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-currency</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-decimal</p></div>
            <div class="col-xs-1 class-name"><p>pricing-card-unit</p></div>
            <div class="col-xs-1 class-name"><p>p.pricing-card-text</p></div>
            <div class="col-xs-1 class-name"><p>ul.pricing-card-details</p></div>
            <div class="col-xs-1 class-name"><p>progress </p></div>
            <div class="col-xs-1 class-name"><p>progress-xs</p></div>
            <div class="col-xs-1 class-name"><p>progress-bar</p></div>
            <div class="col-xs-1 class-name"><p>progress-bar-success</p></div>
            <div class="col-xs-1 class-name"><p>pull-right</p></div>
            <div class="col-xs-1 class-name"><p>small.help-block</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
            <div class="col-xs-1 class-name"><p>xxxxxxxx</p></div>
        </div>

    </div>
</div>
@endsection