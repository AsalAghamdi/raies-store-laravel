@extends('layouts.admin')

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button">اختصار - انشاء صفحة جديدة</button>
            </div>
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">نماذج الفورم</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row">
                <div class="col-md-10">
                    <form class="form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">الاسم</label>
                            <div class="col-sm-9">
                                <input id="form-control-1" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-1">الإيميل</label>
                            <div class="col-sm-3">
                                <div class="input-with-icon">
                                    <input id="form-control-24" class="form-control" type="email" placeholder="الإيميل">
                                    <span class="icon icon-envelope input-icon"></span>
                                </div>
                            </div>

                            
                            <label class="col-sm-3 control-label" for="form-control-22">رفع الملف</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="No file chosen">
                                    <span class="input-group-btn">
                                        <label class="btn btn-primary file-upload-btn">
                                            <input id="form-control-22" class="file-upload-input" type="file"
                                                name="file">
                                            <span class="icon icon-paperclip icon-lg"></span>
                                        </label>
                                    </span>
                                </div>
                                <p class="help-block">
                                    <small>مقاس الصورة 200بكسل / 200بكسل - ليتم عرض الصورة بشكل أفضل</small>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-2">الرمز السري</label>
                            <div class="col-sm-9">
                                <div class="input-with-icon">
                                    <input class="form-control" type="password" placeholder="الرمز السري">
                                    <span class="icon icon-lock input-icon"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-5">غير قابل للتعديل</label>
                            <div class="col-sm-9">
                                <input id="form-control-5" class="form-control" type="email"
                                    value="teddy.wilson@elephant.theme" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-6">الاختيار</label>
                            <div class="col-sm-9">
                                <select id="form-control-6" class="form-control">
                                    <option value="c-plus-plus">C++</option>
                                    <option value="css">CSS</option>
                                    <option value="java">Java</option>
                                    <option value="javascript">JavaScript</option>
                                    <option value="php">PHP</option>
                                    <option value="python">Python</option>
                                    <option value="ruby">Ruby</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-8">النص</label>
                            <div class="col-sm-9">
                                <textarea id="form-control-8" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-8">النص</label>
                            <div class="col-sm-9">
                                <div class="compose">
                                    <div class="compose-body">
                                        <div class="compose-message">
                                            <div class="compose-editor"></div>
                                            <div class="compose-toolbar">
                                                <div class="btn-toolbar" data-role="editor-toolbar">
                                                    <div class="btn-group">
                                                        <div class="btn-group dropup">
                                                            <button class="btn btn-link link-muted" title="Font Size"
                                                                data-toggle="dropdown" type="button">
                                                                <span class="icon icon-text-height"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="fs-Five" data-edit="fontSize 5">Huge</a></li>
                                                                <li><a class="fs-Three" data-edit="fontSize 3">Normal</a></li>
                                                                <li><a class="fs-One" data-edit="fontSize 1">Small</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button class="btn btn-link link-muted" title="Bold (Ctrl/Cmd+B)"
                                                                data-edit="bold" type="button">
                                                                <span class="icon icon-bold"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Italic (Ctrl/Cmd+I)"
                                                                data-edit="italic" type="button">
                                                                <span class="icon icon-italic"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Strikethrough"
                                                                data-edit="strikethrough" type="button">
                                                                <span class="icon icon-strikethrough"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted"
                                                                title="Underline (Ctrl/Cmd+U)" data-edit="underline"
                                                                type="button">
                                                                <span class="icon icon-underline"></span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button class="btn btn-link link-muted" title="Bullet list"
                                                                data-edit="insertunorderedlist" type="button">
                                                                <span class="icon icon-list-ul"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Number list"
                                                                data-edit="insertorderedlist" type="button">
                                                                <span class="icon icon-list-ol"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted"
                                                                title="Reduce indent (Shift+Tab)" data-edit="outdent"
                                                                type="button">
                                                                <span class="icon icon-outdent"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Indent (Tab)"
                                                                data-edit="indent" type="button">
                                                                <span class="icon icon-indent"></span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button class="btn btn-link link-muted"
                                                                title="Align Left (Ctrl/Cmd+L)" data-edit="justifyleft"
                                                                type="button">
                                                                <span class="icon icon-align-left"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Center (Ctrl/Cmd+E)"
                                                                data-edit="justifycenter" type="button">
                                                                <span class="icon icon-align-center"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted"
                                                                title="Align Right (Ctrl/Cmd+R)" data-edit="justifyright"
                                                                type="button">
                                                                <span class="icon icon-align-right"></span>
                                                            </button>
                                                            <button class="btn btn-link link-muted" title="Justify (Ctrl/Cmd+J)"
                                                                data-edit="justifyfull" type="button">
                                                                <span class="icon icon-align-justify"></span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <label class="btn btn-link link-muted file-upload-btn"
                                                                title="Insert picture">
                                                                <span class="icon icon-picture-o"></span>
                                                                <input class="file-upload-input" type="file" name="file"
                                                                    data-edit="insertImage">
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-control-9">رفع الملف</label>
                            <div class="col-sm-9">
                                <input id="form-control-9" type="file" accept="image/*" multiple="multiple">
                                <p class="help-block">
                                    <small>حجم الملف لايزيد عن 200 كيلوبايت</small>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">اختيارات متعددة</label>
                            <div class="col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="mode" checked="checked"> اختيار1
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="message" value="none"> اختيار2
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="message" value="all" checked="checked"> اختيار الكل
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="message" value="some"> رسالة خطأ
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-xs-offset-3">
                                <hr>
                                <button class="btn btn-primary btn-lg" type="button">Oh Hi, Folks!</button>
                                <button class="btn btn-primary btn-md" type="button">Oh Hi, Folks!</button>
                                <button class="btn btn-primary btn-sm" type="button">Oh Hi, Folks!</button>
                                <button class="btn btn-primary btn-xs" type="button">Oh Hi, Folks!</button>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-9 col-xs-offset-3">
                                <hr>
                                <button class="btn  btn-outline-success" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-outline-info" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-outline-default" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-outline-danger" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-outline-warning" type="button">Oh Hi, Folks!</button>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-9 col-xs-offset-3">
                                <hr>
                                <button class="btn  btn-success" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-info" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-default" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-danger" type="button">Oh Hi, Folks!</button>
                                <button class="btn  btn-warning" type="button">Oh Hi, Folks!</button>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-9 col-xs-offset-3">
                                <hr>
                                <button class="btn btn-danger btn-labeled" type="button">
                                    <span class="btn-label">
                                        <span class="icon icon-download icon-lg icon-fw"></span>
                                    </span>
                                    Download ZIP
                                </button>
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-pill active">
                                        <input type="radio" name="day" checked="checked"> Day
                                    </label>
                                    <label class="btn btn-success">
                                        <input type="radio" name="week"> Week
                                    </label>
                                    <label class="btn btn-success btn-pill">
                                        <input type="radio" name="month"> Month
                                    </label>
                                </div>
                                <div class="btn-group btn-group-sm" role="group">
                                    <button class="btn btn-default" type="button">
                                        <span class="icon icon-bold"></span>
                                    </button>
                                    <button class="btn btn-default" type="button">
                                        <span class="icon icon-italic"></span>
                                    </button>
                                    <button class="btn btn-default" type="button">
                                        <span class="icon icon-underline"></span>
                                    </button>
                                </div>
                                <div class="btn-group dropup">
                                    <button class="btn btn-info dropdown-toggle" data-toggle="dropdown" type="button">
                                    <span class="icon icon-share-alt icon-lg icon-fw"></span>
                                    Share
                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">
                                        <div class="media">
                                            <div class="media-left">
                                            <span class="icon icon-user-plus icon-lg icon-fw"></span>
                                            </div>
                                            <div class="media-body">
                                            <span class="d-b">Invite people to collaborate...</span>
                                            <small>People can sync and edit.</small>
                                            </div>
                                        </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                        <div class="media">
                                            <div class="media-left">
                                            <span class="icon icon-link icon-lg icon-fw"></span>
                                            </div>
                                            <div class="media-body">
                                            <span class="d-b">Send link...</span>
                                            <small>People can view.</small>
                                            </div>
                                        </div>
                                        </a>
                                    </li>
                                    </ul>
                                </div>
                                <button class="btn btn-warning btn-icon sq-32" type="button">
                                    <span class="icon icon-bell-o"></span>
                                </button>
                                <button class="btn btn-success btn-icon" title="You have 3 unread notifications." data-container="body" data-placement="bottom" data-toggle="tooltip" type="button">
                                    <span class="icon icon-bell-o sq-32"></span>
                                    <span class="badge badge-above right">3</span>
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection