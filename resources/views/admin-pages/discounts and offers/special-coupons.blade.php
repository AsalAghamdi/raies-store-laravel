@extends('layouts.admin')

@section('title')
القسائم الشرائية الخاصة
@endsection

@section('css')

<style>
    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }

    .tab-content {
        overflow: initial !important;
    }

    textarea,
    input,
    .input-group {
        width: 100% !important;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">القسائم الشرائية الخاصة</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">



                    <form action="">
                        <div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#coupons" data-toggle="tab">
                                        اصدار قسائم لعمليات الشراء
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="coupons">

                                    <table id="demo-datatables-buttons-1" class="table table-bordered table-nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>البيانات</th>
                                                <th>القيمة</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <th>اسم القسيمة:</th>
                                                <th>
                                                    <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <!-- <span class="input-group-addon">%</span> -->
                                                            <input class="form-control" type="text" value="الاسم" aria-label="Euro">
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <th>الوصف:</th>
                                                <th>
                                                    <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <textarea name="" id="" rows="2">
                                                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص 
                                                                </textarea>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <th>قيمة القسيمة:</th>
                                                <th>
                                                    <div class="col-sm-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">SAR</span>
                                                            <input class="form-control" type="text" value="0.89" aria-label="Euro">
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <th>ايقاف القسائم:</th>
                                                <th>
                                                    <div class="col-sm-9">
                                                        <button class="btn btn-danger btn-sm" type="button">تعطيل</button>
                                                    </div>
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div class="row" style="display: flex; justify-content: center; margin-top: 40px;">
                                        <button class="btn btn-primary btn-sm" type="submit">حفظ التعديلات</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Islamic Calendar | DatePicker -->
<script type="text/javascript">
    $(function() {
        $("#hijri-date-input1").hijriDatePicker();
        $("#hijri-date-input2").hijriDatePicker();
    });
</script>
@endsection