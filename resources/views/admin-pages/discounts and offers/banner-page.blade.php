@extends('layouts.admin')

@section('title')
صور شريط الاعلانات
@endsection

@section('css')
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<style>
    .container {
        width: 100% !important;
    }

    .table-sortable tbody tr {
        cursor: move;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button">العودة</button>
            </div>
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">صور شريط الاعلانات</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>سجل استخدام الكوبون</strong>
                        </div>
                        <div class="card-body">

                            <div class="container">
                                <div class="row clearfix">
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        عنوان الصورة
                                                    </th>
                                                    <th class="text-center">
                                                        الرابط
                                                    </th>
                                                    <th class="text-center">
                                                        الصورة
                                                    </th>
                                                    <th class="text-center">
                                                        تاريخ البدء
                                                    </th>
                                                    <th class="text-center">
                                                        تاريخ الانتهاء
                                                    </th>
                                                    <th class="text-center">
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='addr0' data-id="0" class="hidden">
                                                    <td data-name="name">
                                                        <input type="text" name='name0' placeholder='عنوان الاعلان' class="form-control" />
                                                    </td>
                                                    <td data-name="mail">
                                                        <input type="text" name='mail0' placeholder='رابط الاعلان' class="form-control" />
                                                    </td>
                                                    <td data-name="desc">
                                                        <input type="file" accept="image/*" class="form-control">
                                                    </td>
                                                    <td data-name="desc">
                                                        <input type='text' class="form-control" id="hijri-date-input1" value="" />
                                                    </td>
                                                    <td data-name="desc">
                                                        <input type='text' class="form-control" id="hijri-date-input2" value="" />
                                                    </td>
                                                    <td data-name="del">
                                                        <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'><span aria-hidden="true">حذف</span></button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        اسم الصورة
                                                    </td>
                                                    <td>
                                                        الرابط
                                                    </td>
                                                    <td>
                                                        <a href="{{url('admin-product-page')}}">
                                                            <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        -
                                                    </td>
                                                    <td>
                                                        -
                                                    </td>
                                                    <td data-name="del">
                                                        <button name="del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'><span aria-hidden="true">حذف</span></button>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a id="add_row" class="btn btn-primary float-right" style="margin-top: 20px">اضافة صورة</a>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/admin-theme') }}/js/add-row.js"></script>
<!-- Islamic Calendar | DatePicker -->
<script type="text/javascript">
    $(function() {
        $("#hijri-date-input1").hijriDatePicker();
        $("#hijri-date-input2").hijriDatePicker();
    });
</script>
@endsection