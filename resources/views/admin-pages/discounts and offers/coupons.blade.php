@extends('layouts.admin')

@section('title')
الكوبونات
@endsection

@section('css')

<style>
    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }

    .tab-content {
        overflow: initial !important;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">الكوبونات</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>جميع الكوبونات</strong>
                        </div>
                        <div class="card-body">

                            <div class="panel m-b-lg">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#coupons" data-toggle="tab">
                                            جميع الكوبونات
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#create-coupons" data-toggle="tab">
                                            + اضافة كوبون
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="coupons">

                                        <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>اسم الكوبون</th>
                                                    <th>رمز الكوبون</th>
                                                    <th>نسبة الخصم</th>
                                                    <th>يبدأ بتاريخ..</th>
                                                    <th>ينتهي بتاريخ..</th>
                                                    <th>حالة الكوبون</th>
                                                    <th class="text-center">الاجراءات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>اسم الكوبون</td>
                                                    <td>48DH</td>
                                                    <td>20%</td>
                                                    <td>2020/03/1</td>
                                                    <td>2020/03/24</td>
                                                    <td>متاح</td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('coupon-page')}}'">عرض</button>
                                                        <button class="btn btn-danger btn-sm" type="button">حذف</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>اسم الكوبون</td>
                                                    <td>48DH</td>
                                                    <td>20%</td>
                                                    <td>2020/03/1</td>
                                                    <td>2020/03/24</td>
                                                    <td>منتهي</td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('coupon-page')}}'">عرض</button>
                                                        <button class="btn btn-danger btn-sm" type="button">حذف</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>اسم الكوبون</td>
                                                    <td>48DH</td>
                                                    <td>20%</td>
                                                    <td>2020/03/1</td>
                                                    <td>2020/03/24</td>
                                                    <td>لم يبدأ</td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('coupon-page')}}'">عرض</button>
                                                        <button class="btn btn-danger btn-sm" type="button">حذف</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="tab-pane fade" id="create-coupons">

                                        <div class="row" style="display: flex; justify-content: center;">
                                            <div class="col-md-10">
                                                <form class="form form-horizontal">

                                                    <div class="row">
                                                        <strong style="color:red;">يجب ملء الحقل *</strong>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                اسم الكوبون:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input id="form-control-1" class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-2">
                                                                رمز الكوبون:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input id="form-control-2" class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                نسبة الخصم:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">%</span>
                                                                    <input class="form-control" type="text" value="0.89" aria-label="Euro">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                تاريخ البداية:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type='text' class="form-control" id="hijri-date-input1" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                تاريخ الانتهاء:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type='text' class="form-control" id="hijri-date-input2" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="display: flex; justify-content: center; margin-top: 40px;">
                                                        <button class="btn btn-primary btn-sm" type="button">اضافة الكوبون</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Islamic Calendar | DatePicker -->
<script type="text/javascript">
    $(function() {
        $("#hijri-date-input1").hijriDatePicker();
        $("#hijri-date-input2").hijriDatePicker();
    });
</script>
@endsection