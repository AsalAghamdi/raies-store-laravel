@extends('layouts.admin')

@section('title')
الكوبون
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">اسم الكوبون</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>سجل استخدام الكوبون</strong>
                        </div>
                        <div class="card-body">

                            <div class="panel m-b-lg">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#coupons" data-toggle="tab">
                                            بيانات الكوبون
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#create-coupons" data-toggle="tab">
                                            سجل الطلبات
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="coupons">

                                        <div class="row" style="display: flex; justify-content: center;">
                                            <div class="col-md-10">
                                                <form class="form form-horizontal">

                                                    <div class="row">
                                                        <strong style="color:red;">يجب ملء الحقل *</strong>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                اسم الكوبون:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input id="form-control-1" class="form-control" type="text" value="الاسم">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-2">
                                                                رمز الكوبون:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input id="form-control-2" class="form-control" type="text" value="CM45">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                نسبة الخصم:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">%</span>
                                                                    <input class="form-control" type="text" value="47" aria-label="Euro">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                تاريخ البداية:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type='text' class="form-control" id="hijri-date-input1" value="2020\03\03" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-sm-12">
                                                            <label class="col-sm-3 control-label" for="form-control-1">
                                                                تاريخ الانتهاء:
                                                                <span style="color: red">*</span>
                                                            </label>
                                                            <div class="col-sm-9">
                                                                <input type='text' class="form-control" id="hijri-date-input2" value="2020\03\03"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="display: flex; justify-content: center; margin-top: 40px;">
                                                        <button class="btn btn-primary btn-sm" type="button">تعديل البيانات</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="create-coupons">

                                        <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>رقم الطلب</th>
                                                    <th>رقم العميل</th>
                                                    <th>السعر بعد الخصم</th>
                                                    <th>تاريخ العملية</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>1</th>
                                                    <td>
                                                        <a href="{{url('order-page')}}">
                                                            7649b3
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="{{url('order-page')}}">
                                                            393974
                                                        </a>
                                                    </td>
                                                    <td>
                                                        839 SAR
                                                    </td>
                                                    <td>
                                                        2020\02\10
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>






                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection