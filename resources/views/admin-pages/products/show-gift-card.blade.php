@extends('layouts.admin')

@section('title')
المنتجات
@endsection

@section('css')
<style>
    .prevent-click {
        opacity: 0.5;
        pointer-events: none;
    }

    input {
        width: 80px !important;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button" id="addRow" onclick="addRow()">إضافة تصميم</button>
            </div>
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">منتجات الاهداء</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <strong>جميع المنتجات</strong>
                        </div> -->
                        <div class="card-body">

                            <form action="">
                                <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>صورة التصميم</th>
                                            <th>رقم التصميم</th>
                                            <th>وصف التصميم</th>
                                            <th>السعر</th>
                                            <th>عدد الطلبات</th>
                                            <th class="text-center">الاجراءات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>1</th>
                                            <td>
                                                <a href="{{ asset('assets/admin-theme') }}/images/product-image.jpg">
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                </a>
                                            </td>
                                            <td>3703-T</td>
                                            <td>يوم الام</td>
                                            <td>12</td>
                                            <td class="payment-no">
                                                83794
                                            </td>
                                            <td class="text-center">
                                                <button class="btn btn-defualt btn-sm" type="button"><span class="icon icon-times"></span>&nbsp;تعطيل</button>
                                                <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp;حذف</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    const addRow = () => {
        document.querySelector("#addRow").classList.toggle("prevent-click");

        var newRow = $("<tr>");
        var cols = "";

        cols += '<td>-</td>';
        cols += '<td><input type="file" accept="image/*"></td>';
        cols += '<td><input type="text" class="form-control" name="card-no"/></td>';
        cols += '<td><input type="text" class="form-control" name="card-des"/></td>';
        cols += '<td><input type="text" class="form-control" name="card-price"/></td>';
        cols += '<td>-</td>';
        cols += `<td  class="text-center"> <button class="btn btn-primary btn-sm" type="submit"><span class="icon icon-check"></span>&nbsp;حفظ</button>
        <button class="btn btn-defualt btn-sm" type="button" onclick="document.querySelector('#addRow').classList.toggle('prevent-click'); this.parentElement.parentElement.remove();"><span class="icon icon-times"></span>&nbsp;الغاء</button></td>`;

        newRow.append(cols);
        $('#demo-datatables-buttons-1 tbody').append(newRow);
    }
</script>
@endsection