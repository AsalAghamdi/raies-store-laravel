@extends('layouts.admin')

@section('title')
اضافة منتج
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/tab-table.css">
<style>
    .row {
        margin-bottom: 15px;
    }

    textarea {
        width: 100%;
    }

    .title {
        color: var(--admin-primary);
        font-weight: bold;
        margin-bottom: 30px;
    }

    .title::after {
        content: '';
        /* position: absolute; */
        width: 100%;
        height: 1px;
        background-color: var(--admin-primary);
        font-weight: bold;
        margin-top: 5px;
    }

    .price {
        color: var(--admin-primary);
        font-size: 90%;
    }

    .no-padding {
        padding: 0 5px;
    }

    .container {
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">اضافة منتج</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong style="color:red;">يجب ملء الحقل *</strong>
                        </div>
                        <div class="card-body">
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="col-md-10">
                                    <form class="form form-horizontal">

                                        <div class="row title">
                                            صور المنتج
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-3" style="display: flex; justify-content: center;">
                                                <!-- <label class="col-sm-3 control-label" for="form-control-22">صور المنتج</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <input class="form-control" type="text" placeholder="No file chosen">
                                                        <span class="input-group-btn">
                                                            <label class="btn btn-primary file-upload-btn">
                                                                <input id="form-control-22" class="file-upload-input" type="file" name="file">
                                                                <span class="icon icon-paperclip icon-lg"></span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                    <p class="help-block">
                                                        <small>مقاس الصورة ؟؟ بكسل / ؟؟ بكسل - ليتم عرض الصورة بشكل أفضل</small>
                                                    </p>
                                                </div> -->
                                                <label class="file-upload-btn btn btn-primary">
                                                    <span class="icon icon-cloud-upload"></span>
                                                    ارفع صور المنتج
                                                    <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                                </label>
                                            </div>
                                            <div class="form-group col-sm-9">
                                                <ul class="file-list"></ul>
                                            </div>
                                        </div>

                                        <div class="row title">
                                            بيانات المنتج
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label" for="form-control-1">اسم المنتج</label>
                                                <div class="col-sm-9">
                                                    <div>
                                                        <input id="form-control-1" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label" for="form-control-22">رقم المنتج</label>
                                                <div class="col-sm-9">
                                                    <div>
                                                        <input id="form-control-1" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label" for="demo-select2-2">وصف المنتج</label>
                                                <div class="col-sm-9">
                                                    <textarea name="" id="" cols="30" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row title">
                                            بيانات القطعة
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label no-padding" for="form-control-6">نوع المعدن</label>
                                                <div class="col-sm-9 no-padding">
                                                    <select id="form-control-6" class="form-control">
                                                        <option value="">حدد النوع</option>
                                                        <option value="gold">ذهب</option>
                                                        <option value="silver">فضة</option>
                                                        <option value="diamond">الماس</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6" id="select-carat" style="display: none;">
                                                <label class="col-sm-3 control-label no-padding" for="form-control-7">عيار الذهب</label>
                                                <div class="col-sm-9 no-padding">
                                                    <select id="form-control-7" class="form-control">
                                                        <option value="">حدد العيار</option>
                                                        <option value="">18</option>
                                                        <option value="">21</option>
                                                        <option value="">22</option>
                                                        <option value="">24</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label no-padding" for="form-control-1">التصنيف</label>
                                                <div class="col-sm-9 no-padding">
                                                    <select id="form-control-1" class="form-control">
                                                        <option value="">حدد التصنيف</option>
                                                        <option value="">اقراط</option>
                                                        <option value="">سلاسل</option>
                                                        <option value="">اساور</option>
                                                        <option value="">خواتم</option>
                                                        <option value="">اطقم</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="row">
                                            <div class="form-group col-sm-4">
                                                <label class="col-sm-4 control-label no-padding" for="form-control-22">وزن القطعة</label>
                                                <div class="col-sm-8 no-padding">
                                                    <input class="form-control" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label class="col-sm-4 control-label no-padding" for="form-control-22">السعر قبل الضريبة</label>
                                                <div class="col-sm-8 no-padding">
                                                    <input class="form-control" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label class="col-sm-4 control-label no-padding" for="form-control-22">سعر الاجمالي</label>
                                                <div class="col-sm-8 no-padding">
                                                    <input class="form-control" type="text">
                                                    <span>السعر للخواتم يحسب بمقاس 7.5، يختلف السعر بحسب اختيار العميل</span>
                                                </div>
                                            </div>
                                        </div> -->

                                        <div class="row">

                                            <div class="container">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="active"><a href="#contact_01" data-toggle="tab">قطعة</a><span>x</span>
                                                    </li>
                                                    <li><a href="#" class="add-contact">+ اضافة قطعة</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="container">
                                                        <table id="myTable" class=" table order-list">
                                                            <thead>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td>Gmail</td>
                                                                    <td>Phone</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="col-sm-4">
                                                                        <input type="text" name="name" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-4">
                                                                        <input type="mail" name="mail" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-3">
                                                                        <input type="text" name="phone" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-2"><a class="deleteRow"></a>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: left;">
                                                                        <input type="button" class="btn btn-lg btn-block addrow" id="" value="Add Row" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                    <div class="container">
                                                        <table id="myTable" class=" table order-list">
                                                            <thead>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td>Gmail</td>
                                                                    <td>Phone</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="col-sm-4">
                                                                        <input type="text" name="name" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-4">
                                                                        <input type="mail" name="mail" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-3">
                                                                        <input type="text" name="phone" class="form-control" />
                                                                    </td>
                                                                    <td class="col-sm-2"><a class="deleteRow"></a>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: left;">
                                                                        <input type="button" class="btn btn-lg btn-block addrow" id="addrow" value="Add Row" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- <div class="panel m-b-lg" id="price-table">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#tab" data-toggle="tab" aria-expanded="true">
                                                            القطعة الاولى
                                                        </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="javascript:addNewTab();" data-toggle="tab" aria-expanded="false">
                                                            اضافة قطعة
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade active in" id="tab">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>المقاس</th>
                                                                    <th>الوزن</th>
                                                                    <th>سعر الجرام</th>
                                                                    <th>السعر الاجمالي</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <select class="form-control">
                                                                            <option value="">7</option>
                                                                            <option value="">7.5</option>
                                                                            <option value="">8</option>
                                                                            <option value="">8.5</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" type="text">
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" type="text">
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" type="text">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div> -->


                                            <!-- <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>المقاس</th>
                                                        <th>الوزن</th>
                                                        <th>سعر الجرام</th>
                                                        <th>السعر الاجمالي</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <select class="form-control">
                                                                <option value="">7</option>
                                                                <option value="">7.5</option>
                                                                <option value="">8</option>
                                                                <option value="">8.5</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text">
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text">
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> -->
                                        </div>

                                        <div class="row title">
                                            تصنيف المنتج
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label no-padding" for="demo-select2-2">التشكيلة</label>
                                                <div class="col-sm-9 no-padding">
                                                    <select id="demo-select2-2" class="form-control" multiple="multiple">
                                                        <option value="">مناسبات</option>
                                                        <option value="">زواجات</option>
                                                        <option value="">سهرات</option>
                                                        <option value="">اعياد</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-3 control-label no-padding" for="form-control-2">الفئة</label>
                                                <div class="col-sm-9 no-padding">
                                                    <select id="form-control-2" class="form-control">
                                                        <option value="">نساء</option>
                                                        <option value="">اطفال</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="display: flex; justify-content: center; margin-top: 40px;">
                                            <button class="btn btn-primary btn-sm" type="button">اضف المنتج</button>
                                        </div>



                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    let i = 1;
    const addNewTab = (th) => {
        i++;
        const tabLi = `
            <li class="active">
                <a href="#tab${i}" data-toggle="tab" aria-expanded="true">
                    القطعة الاولى
                </a>
            </li>`;

        const tabDiv = `
            <div class="tab-pane fade active in" id="tab${i}">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>المقاس</th>
                            <th>الوزن</th>
                            <th>سعر الجرام</th>
                            <th>السعر الاجمالي</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>
                                <select class="form-control">
                                    <option value="">7</option>
                                    <option value="">7.5</option>
                                    <option value="">8</option>
                                    <option value="">8.5</option>
                                </select>
                            </td>
                            <td>
                                <input class="form-control" type="text">
                            </td>
                            <td>
                                <input class="form-control" type="text">
                            </td>
                            <td>
                                <input class="form-control" type="text">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>`;

        //price-table
        $('#price-table ul').append(tabLi);
        $('#price-table div').append(tabDiv);
    }
</script>
<script>
    $("#form-control-6").change(function() {
        console.log("ddd");

        console.log($("#form-control-6 option:selected").val());

        if ($("#form-control-6 option:selected").val() == 'gold') {
            $('#select-carat').css('display', 'block');
        } else {
            $('#select-carat').css('display', 'none');
        }
        // var str = "";
        // $("select option:selected").each(function() {
        //     str += $(this).text() + " ";
        // });
        // $("div").text(str);
    });
</script>

<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
      <li class="file template-upload fade">
        <div class="file-thumbnail">
          <div class="spinner spinner-default spinner-sm"></div>
        </div>
        <div class="file-info">
          <span class="file-ext">{%= file.ext %}</span>
          <span class="file-name">{%= file.name %}</span>
        </div>
      </li>
      {% } %}
    </script>
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
      <li class="file template-download fade">
        <a class="file-link" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}">
          {% if (file.thumbnailUrl) { %}
          <div class="file-thumbnail" style="background-image: url({%=file.thumbnailUrl%});"></div>
          {% } else { %}
          <div class="file-thumbnail {%=file.thumbnail%}"></div>
          {% } %}
          <div class="file-info">
          <span class="file-ext">{%=file.extension%}</span>
          <span class="file-name">{%=file.filename%}.</span>
          </div>
          </a>
        <button class="file-delete-btn delete" title="Delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" type="button">
          <span class="icon icon-remove"></span>
          </button>
      </li>
      {% } %}
    </script>
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-83990101-1', 'auto');
    ga('send', 'pageview');
</script>

<script src="{{ asset('assets/admin-theme') }}/js/tab-table.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/create-row-table.js"></script>
@endsection