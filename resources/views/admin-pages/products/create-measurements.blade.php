@extends('layouts.admin')

@section('title')
اضافة مقاس
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">اضافة مقاس</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong style="color:red;">يجب ملء الحقل *</strong>
                        </div>
                        <div class="card-body">
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="col-md-10">
                                    <form class="form form-horizontal">

                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-3 control-label" for="form-control-1">
                                                    التصنيف:
                                                    <span style="color: red">*</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <select id="demo-select2-2" class="form-control" multiple="multiple">
                                                        <option value="">حدد التصنيف</option>
                                                        <option value="">اقراط</option>
                                                        <option value="">سلاسل</option>
                                                        <option value="">اساور</option>
                                                        <option value="">خواتم</option>
                                                        <option value="">اطقم</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin: 20px 0;">
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-3 control-label" for="form-control-1">
                                                    صورة دليل المقاسات:
                                                    <span style="color: red">*</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <input id="form-control-9" type="file" accept="image/*" multiple="multiple">
                                                    <p class="help-block">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin: 20px 0;">
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-3 control-label" for="form-control-1">
                                                    صورة كيفية أخذ المقاس:
                                                    <span style="color: red">*</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <input id="form-control-9" type="file" accept="image/*" multiple="multiple">
                                                    <p class="help-block">
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-3 control-label" for="form-control-1">
                                                    ارشادات:
                                                    <span style="color: red">*</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <div>
                                                        <textarea name="" id="" cols="30" rows="3" style="width: 100%;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-3 control-label" for="form-control-1">
                                                    إخلاء مسؤولية:
                                                    <span style="color: red">*</span>
                                                </label>
                                                <div class="col-sm-9">
                                                    <div>
                                                        <textarea name="" id="" cols="30" rows="3" style="width: 100%;"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="display: flex; justify-content: center; margin-top: 40px;">
                                            <button class="btn btn-primary btn-sm" type="button">اضف المقاس</button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection