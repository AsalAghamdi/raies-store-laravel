@extends('layouts.admin')

@section('title')
تحديد الاصناف والمقاسات
@endsection

@section('css')
<style>
    .wid {
        width: 100% !important;
    }

    .title {
        color: var(--admin-primary) !important;
    }

    .fon-size {
        font-size: 18px;
    }

    .margin {
        padding: 0 35px;
    }

    .input-length {
        width: 90px;
    }

    /* ------ */
    td.value .form-elm,
    .cancleEdit {
        display: none;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button" onclick="window.location.href='{{url('admin-create-meas')}}'">إضافة قياس جديد</button>
            </div>
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">تحديد الاصناف والمقاسات</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">
                                <button type="button" class="card-action card-toggler" title="Collapse"></button>

                            </div>
                            <strong>التصنيفات</strong>
                        </div>
                        <div class="card-body" data-toggle="match-height">
                            <table class="table" id="classification">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>اسم التصنيف</th>
                                        <th>عدد المنتجات</th>
                                        <th>تعديل</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <!-- <td>خواتم</td> -->
                                        <td class="value">
                                            <div class="default-value">
                                                خواتم
                                            </div>
                                            <div class="form-elm">
                                                <input type="text" value="خواتم">
                                            </div>
                                        </td>
                                        <td>11,706</td>
                                        <td>
                                            <!-- <button class="btn btn-primary btn-sm" type="button">تعديل</button> -->
                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>خواتم</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>خواتم</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-outline-primary btn-pill wid" type="button" id="classification-btn">اضافة تصنيف جديد</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">
                                <button type="button" class="card-action card-toggler" title="Collapse"></button>
                            </div>
                            <strong>انواع التصاميم</strong>
                        </div>
                        <div class="card-body" data-toggle="match-height">
                            <table class="table" id="collection">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نوع التصميم</th>
                                        <th>عدد المنتجات</th>
                                        <th>تعديل</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>مناسبات</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>سهرات</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>اعياد</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>حفلات</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-outline-primary btn-pill wid" type="button" id="collection-btn">اضافة تصنيف جديد</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">
                                <button type="button" class="card-action card-toggler" title="Collapse"></button>
                            </div>
                            <strong>الفئات</strong>
                        </div>
                        <div class="card-body" data-toggle="match-height">
                            <table class="table" id="categories">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الفئة</th>
                                        <th>عدد المنتجات</th>
                                        <th>تعديل</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>نساء</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>اطفال</td>
                                        <td>11,706</td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button">تعديل</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-outline-primary btn-pill wid" type="button" id="categories-btn">اضافة تصنيف جديد</button>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">
                                <button type="button" class="card-action card-toggler margin click-after-load" title="Collapse"></button>
                                <!-- <button class="btn btn-primary btn-sm" type="button">تعديل</button> -->
                                <button type="button" class="card-action">
                                    <span class="icon icon-pencil-square fon-size"></span>
                                    تعديل
                                </button>
                            </div>
                            <h4>
                                <strong>دليل مقاسات الخواتم</strong>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="col-md-10">
                                    <h3 class="title">دليل المقاسات</h3>
                                    <img class="wid" src="{{ asset('assets/admin-theme')}}/images/size1.jpg" alt="">

                                    <h3 class="title">كيفية اخذ المقاس</h3>
                                    <img class="wid" src="{{ asset('assets/admin-theme')}}/images/size.jpg" alt="">

                                    <h3 class="title">إرشادات</h3>
                                    <h4>
                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                                    </h4>

                                    <h3 class="title">إخلاء مسؤولية</h3>
                                    <h4>
                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                                    </h4>

                                    <div class="row" style="display:flex; justify-content:flex-end; margin-top: 40px;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-actions">
                                <button type="button" class="card-action card-toggler margin click-after-load" title="Collapse"></button>
                                <!-- <button class="btn btn-primary btn-sm" type="button">تعديل</button> -->
                                <button type="button" class="card-action">
                                    <span class="icon icon-pencil-square fon-size"></span>
                                    تعديل
                                </button>
                            </div>
                            <h4>
                                <strong>دليل مقاسات الاساور</strong>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row" style="display: flex; justify-content: center;">
                                <div class="col-md-10">
                                    <h3 class="title">دليل المقاسات</h3>
                                    <img class="wid" src="{{ asset('assets/admin-theme')}}/images/size1.jpg" alt="">

                                    <h3 class="title">كيفية اخذ المقاس</h3>
                                    <img class="wid" src="{{ asset('assets/admin-theme')}}/images/size.jpg" alt="">

                                    <h3 class="title">إرشادات</h3>
                                    <h4>
                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                                    </h4>

                                    <h3 class="title">إخلاء مسؤولية</h3>
                                    <h4>
                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                                    </h4>

                                    <div class="row" style="display:flex; justify-content:flex-end; margin-top: 40px;">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    editValue = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.cancleEdit').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'block';
    }
    cancleEdit = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.editValue').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'block';
    }
</script>
<script>
    $('#classification-btn').click(() => {
        $('#classification-btn').hide();
        $('#collection-btn').hide();
        $('#categories-btn').hide();
        $('#classification tbody').append(`
                                    <tr>
                                        <form>
                                            <td>-</td>
                                            <td>
                                                <input type="text" class="input-length">
                                            </td>
                                            <td>-</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="submit" id="classification-add">اضافة</button>
                                            </td>
                                        </form>
                                    </tr>`);
    })
    $('#classification-add').click(() => {
        $('#classification-btn').show();
        $('#collection-btn').show();
        $('#categories-btn').show();
    });

    $('#collection-btn').click(() => {
        $('#classification-btn').hide();
        $('#collection-btn').hide();
        $('#categories-btn').hide();
        $('#collection tbody').append(`
                                    <tr>
                                        <form>
                                            <td>-</td>
                                            <td>
                                                <input type="text" class="input-length">
                                            </td>
                                            <td>-</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="submit" id="collection-add">اضافة</button>
                                            </td>
                                        </form>
                                    </tr>`);
    })
    $('#collection-add').click(() => {
        $('#classification-btn').show();
        $('#collection-btn').show();
        $('#categories-btn').show();
    });

    $('#categories-btn').click(() => {
        $('#classification-btn').hide();
        $('#collection-btn').hide();
        $('#categories-btn').hide();
        $('#categories tbody').append(`
                                    <tr>
                                        <form>
                                            <td>-</td>
                                            <td>
                                                <input type="text" class="input-length">
                                            </td>
                                            <td>-</td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="submit" id="categories-add">اضافة</button>
                                            </td>
                                        </form>
                                    </tr>`);
    })
    $('#categories-add').click(() => {
        $('#classification-btn').show();
        $('#collection-btn').show();
        $('#categories-btn').show();
    });
</script>
<script>
    $(window).on('load', function() {
        $(".click-after-load").each(function(elm) {
            console.log(elm.tagName);

            $(this).click();
        });
    });
</script>
@endsection