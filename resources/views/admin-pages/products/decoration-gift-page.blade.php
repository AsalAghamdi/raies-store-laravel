@extends('layouts.admin')

@section('title')
تفاصيل المنتج
@endsection

@section('css')
<style>
    .order-btn {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
    }

    .order-btn button {
        margin: 0 10px;
        margin-top: 40px
    }

    .card-title span {
        color: var(--admin-primary);
    }

    .row {
        display: flex;
        justify-content: center;
    }

    .row-pay {
        display: flex;
        justify-content: space-between;
        margin: 5px 0;
        align-items: center;
    }

    .row-pay div:first-child {
        display: flex;
        align-items: baseline;
    }

    .row-pay div:first-child p {
        margin: 0 10px;
    }

    .row-pay div:first-child p:first-child {
        font-weight: bold;
        font-size: 120%;
        margin: 0;
    }

    .price {
        color: var(--admin-primary);
        font-size: 90%;
    }

    .total-price {
        border-top: 1px solid #eee;
        padding-top: 10px;
    }

    .total-price p {
        font-weight: bold;
    }

    .payment-method {
        border-bottom: 1px solid #eee;
        padding: 5px 0;
        margin: 0;
    }

    .payment-method p {
        margin: 0;
    }

    img.card-payment {
        height: 30px;
        width: auto;
        display: block;
    }

    .shipment-steps {
        display: flex;
        flex-wrap: nowrap;
        justify-content: space-around;
    }

    .shipment-steps .step p {
        margin-top: 10px;
        font-weight: bold;
        font-size: 120%;
    }

    .shipment-steps .step:hover .circle {
        transform: scale(1.2, 1.2);
    }

    .shipment-steps .step .circle {
        background: #212025;
        color: #fff;
        opacity: 0.5;
        transition: all .2s ease-in-out;
    }

    .shipment-steps .step.active .circle {
        background: var(--admin-primary);
        color: #fff;
        opacity: 1;
    }

    .shipment-steps .step.pass .circle {
        background: var(--admin-secondary);
        color: #fff;
        opacity: 1;
    }

    .info {
        width: 100%;
    }

    .row-info p:first-child {
        display: block;
        font-weight: bold;
    }

    .info-name {
        /* border-bottom: 1px solid #eee; */
        /* margin-bottom: 15px; */
    }

    .phone-no {
        direction: ltr;
    }

    /* ------------------------------------------------ */
    .file-thumbnail {
        width: 100px;
        height: 100px;
        border-radius: 3px;
    }

    .file {
        width: fit-content !important;
        height: fit-content !important;
    }

    /* ------------------------------------------------ */
    td.value .form-elm,
    .cancleEdit {
        display: none;
    }

    textarea {
        width: 100%;
    }

    select {
        width: fit-content !important;
    }

    /* ------------------------------------------------ */
    .info-table tbody {
        font-size: 14px;
    }

    .info-table tbody td:nth-child(2) {
        width: 20%;
    }

    .info-table tbody td:nth-child(3) {
        width: 80%;
    }

    .tag-table tbody td:nth-child(1) {
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">تفاصيل المنتج</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class=" gutter-xs" style="position: relative;">
                <div class="col-sm-6">
                    <h2>
                        اضافة
                    </h2>
                    <p>
                        2020/03/15
                    </p>
                </div>
                <div class="col-sm-6 order-btn">
                    <button class="btn btn-default btn-sm" type="button"><span class="icon icon-times"></span>&nbsp; تعطيل المنتج</button>
                    <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp; حذف المنتج</button>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="row">
                    <div class="col-md-8">
                        <!-- صور المنتج -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-diamond"></span>&nbsp; صور المنتج</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">

                                <div class="row" style="justify-content: start;">
                                    <div class="file">
                                        <a class="file-link" href="{{asset('assets/admin-theme')}}/images/product-image.jpg">
                                            <div class="file-thumbnail" style="background-image: url({{asset('assets/admin-theme')}}/images/product-image.jpg)"></div>
                                        </a>
                                        <div class="form-group">
                                            <ul class="file-list"></ul>
                                        </div>
                                        <button class="file-delete-btn delete" title="Delete" type="button">
                                            <span class="icon icon-remove"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="row" style="justify-content: flex-end; border-top: 1px solid #eee; padding: 10px;">
                                    <form id="demo-uploader" action="//uploader.madebytilde.com/" method="post" enctype="multipart/form-data">
                                        <div class="form-group" style="margin: 0;">
                                            <label class="file-upload-btn btn btn-primary">
                                                <span class="icon icon-upload"></span>
                                                رفع صور
                                                <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                            </label>
                                        </div>
                                    </form>
                                    <!-- <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-pencil-square"></span>&nbsp; اضافة صورة</button> -->
                                </div>

                            </div>
                        </div>

                        <!-- تفاصيل المنتج -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-align-left"></span>&nbsp; تفاصيل المنتج</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">
                                <table class="table table-borderless table-striped info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>معلومات المنتج</th>
                                            <th>القيمة</th>
                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>الوصف:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى
                                                </div>
                                                <div class="form-elm">
                                                    <textarea rows="3">
                                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى
                                                    </textarea>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>السعر:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    35 SAR
                                                </div>
                                                <div class="form-elm">
                                                    <input type="text" value="35 SAR">
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>


                    </div>
                    <div class="col-md-4" style="position: relative;">
                        <!-- احصائيات -->
                        <div class="card">
                            <div class="card-header">
                                <!-- <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse"></button>
                                    <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button>
                                </div> -->
                                <strong>احصائيات</strong>
                            </div>
                            <div class="card-body" data-toggle="match-height" style="height: 218px;">
                                <ul class="list-group list-group-divided">
                                    <li class="list-group-item">
                                        <div class="media">
                                            <div class="media-middle media-body">
                                                <h6 class="media-heading">
                                                    <span>نسبة الطلبات لإجمالي طلبات الموقع</span>
                                                </h6>
                                                <h4 class="media-heading">67%
                                                    <small>124,029</small>
                                                </h4>
                                            </div>
                                            <div class="media-middle media-right">
                                                <span class="bg-primary circle sq-40">
                                                    <span class="icon icon-check"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="media">
                                            <div class="media-middle media-body">
                                                <h6 class="media-heading">
                                                    <span>عدد مرات الطلب</span>
                                                </h6>
                                                <h4 class="media-heading">21
                                                    <!-- <small>38,875</small> -->
                                                </h4>
                                            </div>
                                            <div class="media-middle media-right">
                                                <span class="bg-primary circle sq-40">
                                                    <span class="icon icon-check"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="media">
                                            <div class="media-middle media-body">
                                                <h6 class="media-heading">
                                                    <span>عدد مرات التفضيل</span>
                                                </h6>
                                                <h4 class="media-heading">639
                                                    <!-- <small>22,214</small> -->
                                                </h4>
                                            </div>
                                            <div class="media-middle media-right">
                                                <span class="bg-primary circle sq-40">
                                                    <span class="icon icon-check"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script>
    editValue = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.cancleEdit').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'block';
    }
    cancleEdit = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.editValue').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'block';
    }
    deleteTag = (btn) => {
        btn.parentElement.parentElement.remove();
    }
</script>
@endsection