@extends('layouts.admin')

@section('title')
الاهداء
@endsection

@section('css')
<style>
    .row {
        padding: 0;
        margin: 0;
        margin-bottom: 15px;
    }

    .title {
        color: var(--admin-primary);
        font-weight: bold;
        /* margin-bottom: 30px; */
        position: relative;
        padding: 0 15px;
    }

    .title::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 1px;
        background-color: var(--admin-primary);
        bottom: 0;
        /* margin-top: 5px; */
    }

    .inside-title {
        padding: 0 30px;
        display: flex;
        align-items: baseline;
    }

    .inside-title p {
        font-size: 120%;
        font-weight: bold;
    }

    .inside-title div {
        margin: 0 10px;
    }

    /*  */
    .card-g {
        border-top-left-radius: 20px;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
        background: var(--admin-secondary);
        box-shadow: 0px 0px 1px 1px;
        height: 300px;
        margin: 20px 15px;
        position: relative;
        padding: 30px;

        color: #f7f7f7;
    }

    .card-g::after {
        content: '';
        /* background-image: url('{{ asset("assets/admin-theme") }}/images/gift-icon.png'); */
        background-position: center;
        background-repeat: no-repeat;
        background-size: 100%;
        width: 150px;
        height: 150px;
        position: absolute;
        top: -20px;
        left: -30px;
        transition: all .2s ease-in-out;
    }

    .card-g:hover:after {
        transform: scale(1.15, 1.15);
    }

    .card-g.card1::after {
        background-image: url('{{ asset("assets/admin-theme") }}/images/gift-icon.png');
    }

    .card-g.card2::after {
        background-image: url('{{ asset("assets/admin-theme") }}/images/gift-icon2.png');
        width: 160px;
        height: 160px;
    }

    .title2 {
        font-size: 25px;
        color: #DEDEDE;
    }

    .card-g-no {
        font-weight: bold;
        font-size: 40px;
    }

    .card-g-no span {
        font-weight: normal;
        font-size: 14px;
        color: var(--admin-primary);
    }

    .card-g-btn {
        position: absolute;
        bottom: 30px;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <!-- <div class="title-bar-actions">
                <button class="btn btn-primary" type="button">إضافة تغليف</button>
            </div> -->
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">الاهداء</span>
            </h1>
        </div>
        <div class="card card-body">
            <div class="row gutter-xs" style="display:flex;justify-content:center;">
                <div class="col-xs-10" style="overflow:hidden;">


                    <div class="col-xs-6">
                        <div class="card-g card1">
                            <div class="title2">
                                نظام التغليف
                            </div>
                            <div class="row card-g-no">
                                479
                                <span>عدد الطالبين للميزة</span>
                            </div>
                            <div class="row card-g-no">
                                75%
                                <span>نسبة استعمال الميزة من اجمالي الطلبات</span>
                            </div>
                            <div class="card-g-btn">
                                <button class="btn btn-primary btn-sm btn-labeled" type="button" onclick="window.location.href='{{url('admin-show-gift')}}'">
                                    <span class="btn-label">
                                        <span class="icon icon-diamond icon-lg icon-fw"></span>
                                    </span>
                                    استعراض علب الاهداء والكماليات
                                </button>
                                <button class="btn btn-danger btn-sm btn-labeled" type="button">
                                    <span class="btn-label">
                                        <span class="icon icon-times icon-lg icon-fw"></span>
                                    </span>
                                    ايقاف الخدمة
                                </button>
                            </div>
                            <!-- <div class=" card-g-no">
                                <a class="btn btn-sm btn-labeled arrow-left arrow-primary" href="#">
                                    استعراض الاضافات
                                    <span class="btn-label btn-label-right">
                                        <span class="icon icon-shopping-cart icon-lg icon-fw"></span>
                                    </span>
                                </a>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="card-g card2">
                            <div class="title2">
                                بطاقات الاهداء
                            </div>
                            <div class="row card-g-no">
                                452
                                <span>عدد الطالبين للميزة</span>
                            </div>
                            <div class="card-g-btn">
                                <button class="btn btn-primary btn-sm btn-labeled" onclick="window.location.href='{{url('show-gift-card')}}'" type="button">
                                    <span class="btn-label">
                                        <span class="icon icon-ticket icon-lg icon-fw"></span>
                                    </span>
                                    عرض تصاميم البطاقات
                                </button>
                                <button class="btn btn-danger btn-sm btn-labeled" type="button">
                                    <span class="btn-label">
                                        <span class="icon icon-times icon-lg icon-fw"></span>
                                    </span>
                                    ايقاف الخدمة
                                </button>
                            </div>
                        </div>
                    </div>















                    <!-- <div class="row">
                        <h4 class="title">نظام التغليف</h4>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>تعطيل نظام التغليف: </p>
                        </div>
                        <div class="col-6">
                            <label class="switch switch-primary">
                                <input class="switch-input" type="checkbox" checked="checked">
                                <span class="switch-track"></span>
                                <span class="switch-thumb"></span>
                            </label>
                        </div>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>استعراض علب الاهداء: </p>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-share-square"></span>&nbsp;استعراض</button>
                        </div>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>اضافة علبة اهداء: </p>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-plus-square"></span>&nbsp;إضافة</button>
                        </div>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>استعراض الاضافات: </p>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-share-square"></span>&nbsp;استعراض</button>
                        </div>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>اضافة مرفقات اضافية للتغليف: </p>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-plus-square"></span>&nbsp;إضافة</button>
                        </div>
                    </div>

                    <div class="row">
                        <h4 class="title">نظام بطاقات الاهداء</h4>
                    </div>

                    <div class="row inside-title">
                        <div class="col-6">
                            <p>تعطيل بطاقات الاهداء: </p>
                        </div>
                        <div class="col-6">
                            <label class="switch switch-primary">
                                <input class="switch-input" type="checkbox" checked="checked">
                                <span class="switch-track"></span>
                                <span class="switch-thumb"></span>
                            </label>
                        </div>
                    </div>
                    <div class="row inside-title">
                        <div class="col-6">
                            <p>استعراض تصاميم البطاقات: </p>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-share-square"></span>&nbsp;استعراض</button>
                        </div>
                    </div> -->











                    <div class="col-xs-6" style="display: none">
                        <div class="card" data-toggle="match-height">
                            <div class="card-header">
                                <strong><span class="icon icon-plus-shopping-bag"></span>&nbsp;نظام التغليف</strong>
                            </div>
                            <div class="card-body">
                                <div class="row inside-title">
                                    <p>تعطيل نظام التغليف: </p>
                                    <div>
                                        <label class="switch switch-primary">
                                            <input class="switch-input" type="checkbox" checked="checked">
                                            <span class="switch-track"></span>
                                            <span class="switch-thumb"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row inside-title">
                                    <p>استعراض علب الاهداء: </p>
                                    <div>
                                        <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-share-square"></span>&nbsp;استعراض</button>
                                    </div>
                                </div>
                                <div class="row inside-title">
                                    <p>اضافة علبة اهداء: </p>
                                    <div>
                                        <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-plus-square"></span>&nbsp;إضافة</button>
                                    </div>
                                </div>
                                <div class="row inside-title">
                                    <p>استعراض الاضافات: </p>
                                    <div>
                                        <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-share-square"></span>&nbsp;استعراض</button>
                                    </div>
                                </div>
                                <div class="row inside-title">
                                    <p>اضافة مرفقات اضافية للتغليف: </p>
                                    <div>
                                        <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-plus-square"></span>&nbsp;إضافة</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" style="display: none">
                        <div class="card" data-toggle="match-height">
                            <div class="card-header">
                                <strong>جميع التغليفات</strong>
                            </div>
                            <div class="card-body">
                                <table id="" class="table table-bordered table-striped table-nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>صورة التغليف</th>
                                            <th>الاسم</th>
                                            <th>السعر</th>
                                            <th>عدد مرات الطلب</th>
                                            <th class="text-center">تفعيل/تعطيل</th>
                                            <th class="text-center">الاجراءات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>1</th>
                                            <td>
                                                <a href="">
                                                    <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/gift-box.jpg" alt="Jessica Brown">
                                                </a>
                                            </td>
                                            <td><a href="">box 2</a></td>
                                            <td>34 SR</td>
                                            <td>748</td>
                                            <td class="text-center">
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td class="text-center">
                                                <!-- <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-external-link"></span></button> -->
                                                <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-trash"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" style="display: none">

                        <div class="card" data-toggle="match-height">
                            <div class="card-header">
                                <strong>خيارات اضافية للتغليف</strong>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped table-nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الصورة</th>
                                            <th>الاسم</th>
                                            <th>السعر</th>
                                            <th>عدد مرات الطلب</th>
                                            <th class="text-center">تفعيل/تعطيل</th>
                                            <th class="text-center">الاجراءات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>1</th>
                                            <td>
                                                <a href="">
                                                    <img class="img-rounded fit" width="50" height="50" src="https://20ayar.com/image/cache/catalog/Products/202-838x1000.jpg" alt="Jessica Brown">
                                                </a>
                                            </td>
                                            <td><a href="">شرائط</a></td>
                                            <td>34 SR</td>
                                            <td>748</td>
                                            <td class="text-center">
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td class="text-center">
                                                <!-- <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-external-link"></span></button> -->
                                                <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-trash"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>2</th>
                                            <td>
                                                <a href="">
                                                    <img class="img-rounded fit" width="50" height="50" src="https://fbcd.co/product-lg/b784fd24cac676352d613ae98582b41f_resize.png" alt="Jessica Brown">
                                                </a>
                                            </td>
                                            <td><a href="">بطاقات التهنئة</a></td>
                                            <td>34 SR</td>
                                            <td>748</td>
                                            <td class="text-center">
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td class="text-center">
                                                <!-- <button class="btn btn-primary btn-xs" type="button"><span class="icon icon-external-link"></span></button> -->
                                                <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-trash"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>
@endsection