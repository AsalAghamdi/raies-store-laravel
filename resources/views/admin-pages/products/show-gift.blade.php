@extends('layouts.admin')

@section('title')
علب الهدايا والكماليات
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <a class="btn btn-primary" href="{{url('create-gift-box')}}">إضافة علبة اهداء</a>
                <a class="btn btn-primary" href="{{url('create-gift-details')}}">إضافة كماليات</a>
            </div>
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">منتجات الاهداء</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <strong>جميع المنتجات</strong>
                        </div> -->
                        <div class="card-body">
                            <div class="panel m-b-lg">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#home-61" data-toggle="tab">
                                            علب الهدايا
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#profile-61" data-toggle="tab">
                                            الكماليات
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="home-61">
                                        <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>صورة المنتج</th>
                                                    <th>رقم المنتج</th>
                                                    <th>اسم المنتج</th>
                                                    <th>السعر</th>
                                                    <th>التصنيف</th>
                                                    <th>عدد مرات الشراء</th>
                                                    <th class="text-center">الاجراءات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>1</th>
                                                    <td>
                                                        <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                    </td>
                                                    <td>3703-T</td>
                                                    <td>خاتم ذهب</td>
                                                    <td>392</td>
                                                    <td>خاتم</td>
                                                    <td class="payment-no">
                                                        83794
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="window.location.href='{{url('gift-page')}}'"><span class="icon icon-pencil"></span>&nbsp;تعديل</button>
                                                        <button class="btn btn-defualt btn-sm" type="button"><span class="icon icon-times"></span>&nbsp;تعطيل</button>
                                                        <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp;حذف</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="profile-61">
                                        <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>صورة</th>
                                                    <th>وصف</th>
                                                    <th>السعر</th>
                                                    <th>عدد مرات الشراء</th>
                                                    <th class="text-center">الاجراءات</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>1</th>
                                                    <td>
                                                        <a href="{{url('admin-product-page')}}">
                                                            <img class="img-rounded fit" width="50" height="50" src="{{ asset('assets/admin-theme') }}/images/product-image.jpg" alt="Jessica Brown">
                                                        </a>
                                                    </td>
                                                    <td>اضافة بطاقة تهنئة</td>
                                                    <td>20 SAR</td>
                                                    <td class="payment-no">
                                                        83794
                                                    </td>
                                                    <td class="text-center">
                                                        <button class="btn btn-primary btn-sm" type="button" onclick="window.location.href='{{url('decoration-gift-page')}}'"><span class="icon icon-pencil"></span>&nbsp;تعديل</button>
                                                        <button class="btn btn-defualt btn-sm" type="button"><span class="icon icon-times"></span>&nbsp;تعطيل</button>
                                                        <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp;حذف</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection