@extends('layouts.admin')

@section('title')
تفاصيل المنتج
@endsection

@section('css')
<style>
    .order-btn {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
    }

    .order-btn button {
        margin: 0 10px;
        margin-top: 40px
    }

    .card-title span {
        color: var(--admin-primary);
    }

    .row {
        display: flex;
        justify-content: center;
    }

    .row-pay {
        display: flex;
        justify-content: space-between;
        margin: 5px 0;
        align-items: center;
    }

    .row-pay div:first-child {
        display: flex;
        align-items: baseline;
    }

    .row-pay div:first-child p {
        margin: 0 10px;
    }

    .row-pay div:first-child p:first-child {
        font-weight: bold;
        font-size: 120%;
        margin: 0;
    }

    .price {
        color: var(--admin-primary);
        font-size: 90%;
    }

    .total-price {
        border-top: 1px solid #eee;
        padding-top: 10px;
    }

    .total-price p {
        font-weight: bold;
    }

    .payment-method {
        border-bottom: 1px solid #eee;
        padding: 5px 0;
        margin: 0;
    }

    .payment-method p {
        margin: 0;
    }

    img.card-payment {
        height: 30px;
        width: auto;
        display: block;
    }

    .shipment-steps {
        display: flex;
        flex-wrap: nowrap;
        justify-content: space-around;
    }

    .shipment-steps .step p {
        margin-top: 10px;
        font-weight: bold;
        font-size: 120%;
    }

    .shipment-steps .step:hover .circle {
        transform: scale(1.2, 1.2);
    }

    .shipment-steps .step .circle {
        background: #212025;
        color: #fff;
        opacity: 0.5;
        transition: all .2s ease-in-out;
    }

    .shipment-steps .step.active .circle {
        background: var(--admin-primary);
        color: #fff;
        opacity: 1;
    }

    .shipment-steps .step.pass .circle {
        background: var(--admin-secondary);
        color: #fff;
        opacity: 1;
    }

    .info {
        width: 100%;
    }

    .row-info p:first-child {
        display: block;
        font-weight: bold;
    }

    .info-name {
        /* border-bottom: 1px solid #eee; */
        /* margin-bottom: 15px; */
    }

    .phone-no {
        direction: ltr;
    }

    /* ------------------------------------------------ */
    .file-thumbnail {
        width: 100px;
        height: 100px;
        border-radius: 3px;
    }

    .file {
        width: fit-content !important;
        height: fit-content !important;
    }

    /* ------------------------------------------------ */
    td.value .form-elm,
    .cancleEdit {
        display: none;
    }

    textarea {
        width: 100%;
    }

    select {
        width: fit-content !important;
    }

    /* ------------------------------------------------ */
    .info-table tbody {
        font-size: 14px;
    }

    .info-table tbody td:nth-child(2) {
        width: 20%;
    }

    .info-table tbody td:nth-child(3) {
        width: 80%;
    }

    .tag-table tbody td:nth-child(1) {
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">تفاصيل المنتج</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class=" gutter-xs" style="position: relative;">
                <div class="col-sm-6">
                    <h2>
                        رقم المنتج: #93720
                    </h2>
                    <p>
                        2020/03/15
                    </p>
                </div>
                <div class="col-sm-6 order-btn">
                    <button class="btn btn-default btn-sm" type="button"><span class="icon icon-times"></span>&nbsp; تعطيل المنتج</button>
                    <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp; حذف المنتج</button>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="row">
                    <div class="col-md-8">
                        <!-- صور المنتج -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-diamond"></span>&nbsp; صور المنتج</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">

                                <div class="row" style="justify-content: start;">
                                    <div class="file">
                                        <a class="file-link" href="{{asset('assets/admin-theme')}}/images/product-image.jpg">
                                            <div class="file-thumbnail" style="background-image: url({{asset('assets/admin-theme')}}/images/product-image.jpg)"></div>
                                        </a>
                                        <div class="form-group">
                                            <ul class="file-list"></ul>
                                        </div>
                                        <button class="file-delete-btn delete" title="Delete" type="button">
                                            <span class="icon icon-remove"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="row" style="justify-content: flex-end; border-top: 1px solid #eee; padding: 10px;">
                                    <form id="demo-uploader" action="//uploader.madebytilde.com/" method="post" enctype="multipart/form-data">
                                        <div class="form-group" style="margin: 0;">
                                            <label class="file-upload-btn btn btn-primary">
                                                <span class="icon icon-upload"></span>
                                                رفع صور
                                                <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                            </label>
                                        </div>
                                    </form>
                                    <!-- <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-pencil-square"></span>&nbsp; اضافة صورة</button> -->
                                </div>

                            </div>
                        </div>

                        <!-- تفاصيل المنتج -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-align-left"></span>&nbsp; تفاصيل المنتج</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">
                                <table class="table table-borderless table-striped info-table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>معلومات المنتج</th>
                                            <th>القيمة</th>
                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>رقم المنتج:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    R38047-4
                                                </div>
                                                <div class="form-elm">
                                                    <input type="text" value="R38047-4">
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>اسم المنتج:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    خاتم ذهب
                                                </div>
                                                <div class="form-elm">
                                                    <input type="text" value="خاتم ذهب">
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>وصف المنتج:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى
                                                </div>
                                                <div class="form-elm">
                                                    <textarea rows="3">
                                                        هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى
                                                    </textarea>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>الفئة:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    نساء
                                                </div>
                                                <div class="form-elm">
                                                    <select id="form-control-1" class="form-control">
                                                        <option value="">نساء</option>
                                                        <option value="">اطفال</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>التشكيلة:</td>
                                            <td class="value">
                                                <div class="default-value">
                                                    مناسبات، سهرت
                                                </div>
                                                <div class="form-elm">
                                                    <select id="demo-select2-2" class="form-control" multiple="multiple">
                                                        <option value="">مناسبات</option>
                                                        <option value="">زواجات</option>
                                                        <option value="">سهرات</option>
                                                        <option value="">اعياد</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>

                        <!-- بيانات القطعة -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-diamond"></span>&nbsp; بيانات القطعة</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">

                                <div class="panel m-b-lg">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#home-61" data-toggle="tab" aria-expanded="true">
                                                القطعة الاولى
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#profile-61" data-toggle="tab" aria-expanded="false">
                                                القطعة الثانية
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="home-61">
                                            <table class="table table-borderless table-striped info-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>معلومات القطعة</th>
                                                        <th>القيمة</th>
                                                        <th>تعديل</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>نوع المعدن:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                ذهب
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                    <!-- TODO1: this row only visible if the product was gold-->
                                                    <tr>
                                                        <td></td>
                                                        <td>عيار الذهب:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                21 قيراط
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>تصنيف القطعة:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                خاتم
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="profile-61">
                                            <table class="table table-borderless table-striped info-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>معلومات القطعة</th>
                                                        <th>القيمة</th>
                                                        <th>تعديل</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>نوع المعدن:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                ذهب
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                    <!-- TODO1: this row only visible if the product was gold-->
                                                    <tr>
                                                        <td></td>
                                                        <td>عيار الذهب:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                21 قيراط
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>تصنيف القطعة:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                خاتم
                                                            </div>
                                                        </td>
                                                        <td>
                                                            -
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- السعر -->
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-dollar"></span>&nbsp; تسعيرة المنتج</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">



                                <div class="panel m-b-lg">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab2" data-toggle="tab" aria-expanded="true">
                                                القطعة الاولى
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab1" data-toggle="tab" aria-expanded="false">
                                                القطعة الثانية
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab2">
                                            <table class="table table-borderless table-striped info-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>المقاس</th>
                                                        <th>الوزن</th>
                                                        <th>سعر الجرام</th>
                                                        <th>السعر الاجمالي</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>7</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                7
                                                            </div>
                                                            <div class="form-elm">
                                                                <input type="text" name="" id="" value="7">
                                                            </div>
                                                        </td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                600 جرام
                                                            </div>
                                                            <div class="form-elm">
                                                                <input type="text" name="" id="" value="600 جرام">
                                                            </div>
                                                        </td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                238 SAR
                                                            </div>
                                                            <div class="form-elm">
                                                                <input type="text" name="" id="" value="238 SAR">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>السعر قبل الضريبة:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                804 SAR
                                                            </div>
                                                            <div class="form-elm">
                                                                <textarea rows="3">
                                                    804 SAR
                                                    </textarea>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>السعر الاجمالي:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                894 SAR
                                                            </div>
                                                            <div class="form-elm">
                                                                <textarea rows="3">
                                                    894 SAR
                                                    </textarea>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab1">
                                            <table class="table table-borderless table-striped info-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>معلومات المنتج</th>
                                                        <th>القيمة</th>
                                                        <th>تعديل</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>الوزن:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                600 جرام
                                                            </div>
                                                            <div class="form-elm">
                                                                <textarea rows="3">
                                                        600 جرام
                                                    </textarea>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>السعر قبل الضريبة:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                804 SAR
                                                            </div>
                                                            <div class="form-elm">
                                                                <textarea rows="3">
                                                    804 SAR
                                                    </textarea>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>السعر الاجمالي:</td>
                                                        <td class="value">
                                                            <div class="default-value">
                                                                894 SAR
                                                            </div>
                                                            <div class="form-elm">
                                                                <textarea rows="3">
                                                    894 SAR
                                                    </textarea>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-default btn-xs editValue" onclick="editValue(this)" type="button"><span class="icon icon-pencil"></span></button>
                                                            <button class="btn btn-danger btn-xs cancleEdit" onclick="cancleEdit(this)" type="button"><span class="icon icon-remove"></span></button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- TODO: يظهر بحال كان المنتج عبارة عن طقم -->
                                <!-- السعر الاجمالي للطقم -->
                                <div class="row" style="justify-content: flex-end; border-top: 1px solid #eee; padding: 10px;width: 100%; margin: 0; font-weight: bold;">
                                    <p>السعر الاجمالي للطقم:</p>
                                    &nbsp;
                                    &nbsp;
                                    <p>4893 SAR</p>
                                </div>



                            </div>
                        </div>


                    </div>
                    <div class="col-md-4" style="position: relative;">
                        <!-- Tags -->
                        <div class="card info">
                            <div class="card-header">
                                <h4 class="card-title">
                                    <strong><span class="icon icon-tags"></span>&nbsp; الوسوم</strong>
                                </h4>
                            </div>
                            <div class="card-body" data-toggle="match-height">
                                <!-- TODO2: الوسوم تنحذف بالواجهة فقط، عشان نقدر نحذفها بالDB حاول تعيد تعيين كل الوسوم من جديد  -->
                                <table class="table table-borderless table-striped tag-table">
                                    <thead>
                                        <tr>
                                            <th>اسم الوسم</th>
                                            <th>حذف</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>وسم 1</td>
                                            <input type="hidden" value="وسم 1">
                                            <td>
                                                <button class="btn btn-danger btn-xs" onclick="deleteTag(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>وسم 2</td>
                                            <input type="hidden" value="وسم 2">
                                            <td>
                                                <button class="btn btn-danger btn-xs" onclick="deleteTag(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>وسم 3</td>
                                            <input type="hidden" value="وسم 3">
                                            <td>
                                                <button class="btn btn-danger btn-xs" onclick="deleteTag(this)" type="button"><span class="icon icon-remove"></span></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row" style="justify-content: flex-end; border-top: 1px solid #eee; padding: 10px;width: 100%; margin: 0;">
                                <button class="btn btn-primary btn-sm" type="button"><span class="icon icon-pencil-square"></span>&nbsp;اضافة وسم</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script>
    editValue = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.cancleEdit').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'block';
    }
    cancleEdit = (btn) => {
        btn.style.display = 'none';
        btn.parentElement.querySelector('.editValue').style.display = 'block';

        btn.parentElement.parentElement.querySelector('.value').querySelector('.form-elm').style.display = 'none';
        btn.parentElement.parentElement.querySelector('.value').querySelector('.default-value').style.display = 'block';
    }
    deleteTag = (btn) => {
        btn.parentElement.parentElement.remove();
    }
</script>
@endsection