@extends('layouts.admin')

@section('content')
<div class="layout-content">
    <div class="layout-content-body">

        <!-- عنوان الصفحة -->
        <div class="title-bar">
            <!-- ازارير الإختصارات -->
            <div class="title-bar-actions">
                <button class="btn btn-primary" type="button">اختصار - انشاء صفحة جديدة</button>
            </div>
            <!-- العنوان -->
            <h1 class="title-bar-title">
                <span class="d-ib">الرسوم البيانية</span>
            </h1>
        </div>
          
        <div class="row gutter-xs">
            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas 
                        data-chart="line" 
                        data-labels='["May", "Jun", "Jul", "Aug"]' 
                        data-values='[
                            {
                                "backgroundColor": "transparent", 
                                "borderColor": "#50b432", 
                                "label": "Visitors", 
                                "data": [60285, 50687, 56529, 49634]
                            }]' 
                            data-hide='["legend"]' 
                            height="150" 
                            width="300">
                    </canvas>
                    <h6 class="m-b-0">Overview of Audience</h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas 
                        data-chart="line" 
                        data-labels='["01 Jun, 2016", "02 Jun, 2016", "03 Jun, 2016", "04 Jun, 2016", "05 Jun, 2016", "06 Jun, 2016", "07 Jun, 2016", "08 Jun, 2016", "09 Jun, 2016", "10 Jun, 2016", "11 Jun, 2016", "12 Jun, 2016", "13 Jun, 2016", "14 Jun, 2016", "15 Jun, 2016", "16 Jun, 2016", "17 Jun, 2016", "18 Jun, 2016", "19 Jun, 2016", "20 Jun, 2016", "21 Jun, 2016", "22 Jun, 2016", "23 Jun, 2016", "24 Jun, 2016", "25 Jun, 2016", "26 Jun, 2016", "27 Jun, 2016", "28 Jun, 2016", "29 Jun, 2016", "30 Jun, 2016"]' 
                        data-values='[
                            {
                                "backgroundColor": "rgba(80, 180, 50, 0.2)", 
                                "borderColor": "transparent",
                                "label": "New Visitor",
                                "data": [325, 427, 399, 458, 745, 587, 758, 754, 710, 841, 645, 548, 645, 651, 645, 712, 631, 754, 611, 645, 500, 490, 565, 751, 654, 658, 456, 445, 701, 711]
                            },
                            {
                                "backgroundColor": "rgba(5, 141, 199, 0.2)", 
                                "borderColor": "transparent", 
                                "label": "Returning Visitor", 
                                "data": [750, 772, 871, 1011, 1136, 985, 1003, 897, 945, 975, 957, 1070, 1121, 1065, 1222, 1065, 1235, 1312, 1165, 1135, 1130, 1136, 1009, 1035, 1003, 981, 965, 1098, 958, 1205]
                            }
                            ]' 
                            data-hide='["scalesX"]' 
                            height="150" width="300">
                    </canvas>
                    <h6 class="m-b-0">Overview of Audience</h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas data-chart="line"
                        data-labels='["01 Jun, 2016", "02 Jun, 2016", "03 Jun, 2016", "04 Jun, 2016", "05 Jun, 2016", "06 Jun, 2016", "07 Jun, 2016", "08 Jun, 2016", "09 Jun, 2016", "10 Jun, 2016", "11 Jun, 2016", "12 Jun, 2016", "13 Jun, 2016", "14 Jun, 2016", "15 Jun, 2016", "16 Jun, 2016", "17 Jun, 2016", "18 Jun, 2016", "19 Jun, 2016", "20 Jun, 2016", "21 Jun, 2016", "22 Jun, 2016", "23 Jun, 2016", "24 Jun, 2016", "25 Jun, 2016", "26 Jun, 2016", "27 Jun, 2016", "28 Jun, 2016", "29 Jun, 2016", "30 Jun, 2016"]'
                        data-values='[
                            {
                                "backgroundColor": "rgba(80, 180, 50, 0.2)", 
                                "borderColor": "#50b432", 
                                "borderWidth": 1, 
                                "label": "New Visitor", 
                                "data": [325, 427, 399, 458, 745, 587, 758, 754, 710, 841, 645, 548, 645, 651, 645, 712, 631, 754, 611, 645, 500, 490, 565, 751, 654, 658, 456, 445, 701, 711]
                            }, 
                            {
                                "backgroundColor": "rgba(5, 141, 199, 0.2)", 
                                "borderColor": "#058dc7", 
                                "borderWidth": 2, 
                                "label": "Returning Visitor", 
                                "data": [750, 772, 871, 1011, 1136, 985, 1003, 897, 945, 975, 957, 1070, 1121, 1065, 1222, 1065, 1235, 1312, 1165, 1135, 1130, 1136, 1009, 1035, 1003, 981, 965, 1098, 958, 1205]
                            }]'
                        data-hide='[ "points"]' 
                        height="150" width="300">
                    </canvas>
                    <h6 class="m-b-0">Overview of Audience</h6>
                </div>
            </div>
            
            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas 
                    data-chart="line" 
                    data-labels='["May", "Jun", "Jul", "Aug"]'
                    data-values='[
                        {
                            "backgroundColor": "rgba(80, 180, 50, 0.2)", 
                            "borderColor": "#50b432", 
                            "borderWidth": 2, 
                            "pointBackgroundColor": "#50b432", 
                            "pointRadius": 1, 
                            "label": "Visitors", 
                            "data": [60285, 50687, 56529, 49634]
                        }]'
                    data-hide='["gridLinesX", "legend"]' 
                    height="150" width="300">
                    </canvas>
                    <h6 class="m-b-0">Overview of Audience</h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas data-chart="bar" data-labels='["May", "Jun", "Jul", "Aug"]'
                        data-values='[{"backgroundColor": "rgba(80, 180, 50, 0.2)", "borderColor": "#50b432", "borderWidth": 1, "label": "Visitors", "data": [60285, 50687, 56529, 49634]}]'
                        data-hide='["legend"]' data-scales='{"xAxes": [{ "barPercentage": 0.5 }]}' height="150"
                        width="300"></canvas>
                    <h6 class="m-b-0">Overview of Audience</h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas data-chart="bar" data-labels='["May", "Jun", "Jul", "Aug"]'
                        data-values='[{"backgroundColor": "rgba(80, 180, 50, 0.2)", "borderColor": "#50b432", "borderWidth": 1, "label": "New Visitor", "data": [10489, 14790, 20332, 18422]}, {"backgroundColor": "rgba(5, 141, 199, 0.2)", "borderColor": "#058dc7", "borderWidth": 1, "label": "Returning Visitor", "data": [29174, 35897, 36197, 31212]}]'
                        data-scales='{"yAxes": [{ "ticks": { "beginAtZero": "ture" }}]}' data-hide='["  "]'
                        height="150" width="300"></canvas>
                    <h6 class="m-b-0">Overview of ءء</h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <canvas 
                        data-chart="horizontal-bar" 
                        data-labels='["May", "Jun", "Jul", "Aug"]'
                        data-values='[
                            {
                                "backgroundColor": "#50b432", 
                                "label": "New Visitor", 
                                "data": [10489, 14790, 20332, 18422]
                            }, 
                            {
                                "backgroundColor": "#058dc7", 
                                "label": "Returning Visitor", 
                                "data": [29174, 35897, 36197, 31212]
                            }]'
                        data-scales='{"xAxes": [{ "ticks": { "beginAtZero": "ture" }}]}' 
                        data-hide='[""]'
                        height="150" width="300">
                    </canvas>
                    <h6 class="m-b-0">
                        Browsers used by visitors
                    </h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body text-center" data-toggle="match-height">
                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-3">
                            <canvas data-chart="doughnut" data-labels='["Returning Visitor", "New Visitor"]'
                                data-values='[{"backgroundColor": ["#058dc7", "#50b432"], "data": [132480, 64033]}]'
                                data-legend='{"position": "bottom"}' 
                                data-hide='["scalesX", "scalesY"]' data-cutout-percentage="80" height="150"
                                width="150"></canvas>
                        </div>
                    </div>
                    <h6 class="m-b-0">
                        Browsers used by visitors
                    </h6>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body" data-toggle="match-height">
                    <h6 class="text-center m-t-0">Browsers used by visitors</h6>
                    <div class="row">
                        <div class="col-xs-3">
                            <ul class="list-unstyled">
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #80bf41"></span>
                                        Chrome
                                    </small>
                                </li>
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #f2d548"></span>
                                        Firefox
                                    </small>
                                </li>
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #0359a5"></span>
                                        IE
                                    </small>
                                </li>
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #e86203"></span>
                                        Safari
                                    </small>
                                </li>
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #d9184b"></span>
                                        Opera
                                    </small>
                                </li>
                                <li class="m-b">
                                    <small class="nowrap">
                                        <span class="icon icon-square icon-fw" style="color: #ededed"></span>
                                        Others
                                    </small>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-6">
                            <canvas data-chart="pie"
                                data-labels='["Chrome", "Firefox", "IE", "Safari", "Opera", "Others"]'
                                data-values='[{"backgroundColor": ["#80bf41", "#f2d548", "#0359a5", "#e86203", "#d9184b", "#ededed"], "data": [137363, 34979, 11987, 7074, 2555, 2555]}]'
                                data-hide='["scalesX", "scalesY", "legend"]' height="300" width="300"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <!-- chart -->
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-body" data-toggle="match-height">
                    <h6 class="text-center m-t-0">
                        Overview of Audience - Age
                    </h6>
                    <div class="fh">
                        <div class="fh-m">
                            <div class="row">
                                <div class="col-xs-3">
                                    <ul class="list-unstyled">
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #f7464a"></span>
                                                18-24
                                            </small>
                                        </li>
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #46bfbd"></span>
                                                25-34
                                            </small>
                                        </li>
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #fdb45c"></span>
                                                35-44
                                            </small>
                                        </li>
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #949fb1"></span>
                                                45-54
                                            </small>
                                        </li>
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #795548"></span>
                                                55-64
                                            </small>
                                        </li>
                                        <li class="m-b">
                                            <small class="nowrap">
                                                <span class="icon icon-square icon-fw" style="color: #4d5360"></span>
                                                65+
                                            </small>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-6">
                                    <canvas data-chart="polar-area"
                                        data-labels='["18-24", "25-34", "35-44", "45-54", "55-64", "65+"]'
                                        data-values='[{"backgroundColor": ["#f7464a", "#46bfbd", "#fdb45c", "#949fb1", "#795548", "#4d5360"], "data": [54041, 65832, 30460, 24564, 10808, 10808]}]'
                                        data-scale='{"ticks": {"beginAtZero": "true"}}'
                                        data-hide='["scalesX", "scalesY", "legend"]' height="300" width="300"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection