@extends('layouts.admin')

@section('title')
عضويات العملاء
@endsection

@section('css')
<style>
    .order-btn {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
    }

    .order-btn button {
        margin: 0 10px;
        margin-top: 40px
    }

    .card-title span {
        color: var(--admin-primary);
    }

    .container::before {
        content: "مراحل الطلب";
        position: absolute;

    }

    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <!-- <h1 class="title-bar-title">
                <span class="d-ib">عضويات العملاء</span>
            </h1> -->
        </div>
        <div class="card card-body p30px">
            <div class=" gutter-xs" style="position: relative;">
                <div class="col-sm-6">
                    <h2>
                        عضويات العملاء
                    </h2>
                </div>
                <div class="col-sm-6 order-btn">
                    <a class="btn btn-primary btn-sm" href="{{url('create-category')}}"><span class="icon icon-plus"></span>&nbsp; انشاء عضوية جديدة</a>
                </div>
            </div>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">


                    <div class="panel m-b-lg">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#home-61" data-toggle="tab">
                                    العضوية 1
                                </a>
                            </li>
                            <li>
                                <a href="#profile-61" data-toggle="tab">
                                    العضوية 2
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="home-61">
                                <form action="">
                                    <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>بيانات العضوية</th>
                                                <th>القيمة</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>اسم العضوية:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="اسم العضوية">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>الحد الادنى لإجمالي المشتريات:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="1000 SAR">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>الحد الاعلى لإجمالي المشتريات:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="3000 SAR">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row" style="display:flex; justify-content:center; margin: 20px 0;">
                                        <button class="btn btn-primary btn-sm" type="submit"><span class="icon icon-pencil-square"></span>&nbsp;حفظ التعديلات</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="profile-61">
                                <form action="">
                                    <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>بيانات العضوية</th>
                                                <th>القيمة</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>اسم العضوية:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="اسم العضوية">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>الحد الادنى لإجمالي المشتريات:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="1000 SAR">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>الحد الاعلى لإجمالي المشتريات:</td>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="3000 SAR">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row" style="display:flex; justify-content:center; margin: 20px 0;">
                                        <button class="btn btn-primary btn-sm" type="submit"><span class="icon icon-pencil-square"></span>&nbsp;حفظ التعديلات</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection