@extends('layouts.admin')

@section('title')
العملاء
@endsection

@section('css')
<style>
    .container::before {
        content: "مراحل الطلب";
        position: absolute;

    }

    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">العملاء</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>جميع العملاء</strong>
                        </div>
                        <div class="card-body">
                            <table id="demo-datatables-buttons-1" class="table table-bordered table-striped table-nowrap dataTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>رقم العميل</th>
                                        <th>الاسم</th>
                                        <th>الايميل</th>
                                        <th>تاريخ التسجيل</th>
                                        <th>العضوية</th>
                                        <th class="text-center">استعراض</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>4794545</td>
                                        <td>اسم العميل</td>
                                        <td>cus@hotmail.com</td>
                                        <td>2020/3/5</td>
                                        <td>العضوية الذهبية</td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('customer-page')}}'">تفاصيل اكثر</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>4794545</td>
                                        <td>اسم العميل</td>
                                        <td>cus@hotmail.com</td>
                                        <td>2020/3/5</td>
                                        <td>العضوية الذهبية</td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('customer-page')}}'">تفاصيل اكثر</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>4794545</td>
                                        <td>اسم العميل</td>
                                        <td>cus@hotmail.com</td>
                                        <td>2020/3/5</td>
                                        <td>العضوية الذهبية</td>
                                        <td class="text-center">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="location.href='{{url('customer-page')}}'">تفاصيل اكثر</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection