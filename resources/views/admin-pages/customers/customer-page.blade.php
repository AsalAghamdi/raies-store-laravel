@extends('layouts.admin')

@section('title')
صفحة العميل
@endsection

@section('css')
<style>
    .order-btn {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
    }

    .order-btn button {
        margin: 0 10px;
        margin-top: 40px
    }

    .card-title span {
        color: var(--admin-primary);
    }

    .container::before {
        content: "مراحل الطلب";
        position: absolute;

    }

    .steps {
        display: flex;
        justify-content: space-between;
    }

    .steps div {
        width: 33.33%;
    }
</style>
@endsection

@section('content')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <!-- page title -->
            <h1 class="title-bar-title">
                <span class="d-ib">صفحة العميل</span>
            </h1>
        </div>
        <div class="card card-body p30px">
            <div class=" gutter-xs" style="position: relative;">
                <div class="col-sm-6">
                    <h2>
                        رقم العميل: #83734
                    </h2>
                    <p>
                        تاريخ التسجيل: 2020/03/15
                    </p>
                </div>
                <div class="col-sm-6 order-btn">
                    <button class="btn btn-danger btn-sm" type="button"><span class="icon icon-trash"></span>&nbsp; حذف الحساب</button>

                </div>
            </div>
        </div>
        <div class="card card-body p30px">
            <div class="row gutter-xs">
                <div class="col-xs-12">


                    <div class="panel m-b-lg">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#home-61" data-toggle="tab">
                                    عام
                                </a>
                            </li>
                            <li>
                                <a href="#profile-61" data-toggle="tab">
                                    سجل الطلبات
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#dropdown-61" data-toggle="tab">
                                    عمليات الاسترجاع
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#dropdown-1" data-toggle="tab">
                                    النقاط المكتسبة
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#coupons" data-toggle="tab">
                                    القسائم الشرائية المكتسبة
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="home-61">
                                <form action="">
                                    <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>معلومات العميل</th>
                                                <th>القيمة</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <th>اسم العميل</th>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="اسم العميل">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <th>البريد الالكتروني</th>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="cus@hotmail.com">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <th>رقم الجوال</th>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="+966000000000">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <th>العضوية</th>
                                                <td>
                                                    <input class="form-control input-contrast" type="text" value="العضوية الذهبية">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row" style="display:flex; justify-content:center; margin: 20px 0;">
                                        <button class="btn btn-primary btn-sm" type="submit"><span class="icon icon-pencil-square"></span>&nbsp;حفظ التعديلات</button>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane fade" id="profile-61">
                                <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>رقم الطلب</th>
                                            <th>التاريخ</th>
                                            <th>السعر الاجمالي</th>
                                            <th>استعراض</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('order-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('order-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('order-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('order-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="dropdown-61">
                                <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>رقم الطلب</th>
                                            <th>التاريخ</th>
                                            <th>السعر الاجمالي</th>
                                            <th>استعراض</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('return-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <th>489303</th>
                                            <th>2019/5/7</th>
                                            <th>7403 SAR</th>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{url('return-page')}}">عرض الطلب</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="dropdown-1">
                                <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>تاريخ الاستحقاق</th>
                                            <th>الوصف</th>
                                            <th>عدد النقاط</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>2019/5/7</td>
                                            <td>-</td>
                                            <td>370 نقطة</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>2019/5/7</td>
                                            <td>-</td>
                                            <td>370 نقطة</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="coupons">
                                <table id="demo-datatables-buttons-1" class="table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>اسم القسيمة</th>
                                            <th>الوصف</th>
                                            <th>المبلغ</th>
                                            <th>الاجراء</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>اسم القسيمة</td>
                                            <td>-</td>
                                            <td>370 SAR</td>
                                            <td>
                                                <button class="btn btn-danger btn-sm">حذف القسيمة</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection