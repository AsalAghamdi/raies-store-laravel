<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <!-- smo -->
    @include('admin-partials.smo')
    @yield('smo')

    @include('admin-partials.head')

    <!-- title -->
    <title>الريس | @yield('title')</title>
    <!-- custom CSS -->
    @yield('css')

</head>

<body class="layout layout-header-fixed">

    <!-- الهيدر -->
    @include('admin-partials.header')

    <div class="layout-main">
        
        <!-- sidebar -->
        @include('admin-partials.sidebar')
        
        <!-- content -->
        @yield('content')
        
        <!-- footer -->
        @include('admin-partials.footer')

    </div>
    <!-- theme option -->
    @include('admin-partials.theme-option')
    <!-- theme javascripts -->
    @include('admin-partials.scripts')
    <!-- custom javascript -->
    @yield('js')
</body>

</html>