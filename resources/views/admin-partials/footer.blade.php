
<div class="layout-footer bg-primary">
    <div class="layout-footer-body">
        <small class="version">تصميم وبرمجة 
            <a href="https://www.i-esnaad.com" >i-Esnaad</a>
        </small>
        <small class="copyright">2020 &copy; 
            جميع الحقوق محفوظة لدى الشركة
        </small>
    </div>
</div>