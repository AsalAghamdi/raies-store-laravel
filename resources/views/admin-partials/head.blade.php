<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">

<meta name="description" content="موقع الإسناد الرقمي">

<link rel="apple-touch-icon" sizes="180x180" href="">
<link rel="icon" type="image/png" href="{{ asset('assets/admin-theme') }}/images/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="{{ asset('assets/admin-theme') }}/images/favicon-16x16.png" sizes="16x16">

<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
<meta name="theme-color" content="#ffffff">

<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/vendor-rtl.min.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/elephant-rtl.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/application-rtl.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/profile-rtl.min.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/demo-rtl.min.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/css/compose-rtl.min.css">
<link rel="stylesheet" href="{{ asset('assets/admin-theme') }}/style.css">