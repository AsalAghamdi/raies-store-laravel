
<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header">

            <!-- شعار لوحة التحكم -->
            <a class="navbar-brand navbar-brand-center" href="index.html">
                <!-- image -->
                <img class="navbar-brand-logo" src="{{ asset('assets/admin-theme') }}/images/logo-inverse.svg" alt="الاسناد">
            </a>

            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse"
                data-target="#sidenav">
                <span class="sr-only">Toggnavigationle </span>
                <span class="bars">
                    <span class="bar-line bar-line-1 out"></span>
                    <span class="bar-line bar-line-2 out"></span>
                    <span class="bar-line bar-line-3 out"></span>
                </span>
                <span class="bars bars-x">
                    <span class="bar-line bar-line-4"></span>
                    <span class="bar-line bar-line-5"></span>
                </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse"
                data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="arrow-up"></span>
                <span class="ellipsis ellipsis-vertical">
                    <!-- image -->
                    <img class="ellipsis-object" width="32" height="32" src="{{ asset('assets/admin-theme') }}/images/user.jpg" alt="الاسناد">
                </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">

                <!-- زر القوائم الجانبية -->
                <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true"
                    type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="bars">
                        <span class="bar-line bar-line-1 out"></span>
                        <span class="bar-line bar-line-2 out"></span>
                        <span class="bar-line bar-line-3 out"></span>
                        <span class="bar-line bar-line-4 in"></span>
                        <span class="bar-line bar-line-5 in"></span>
                        <span class="bar-line bar-line-6 in"></span>
                    </span>
                </button>

                <!-- عناصر الهيدر يسار -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="visible-xs-block">
                        <h4 class="navbar-text text-center">المدير</h4>
                    </li>

                    <!-- messages -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true">
                            <span class="icon-with-child hidden-xs">
                                <span class="icon icon-envelope-o icon-lg"></span>
                                <span class="badge badge-danger badge-above right">8</span>
                            </span>
                            <span class="visible-xs-block">
                                <span class="icon icon-envelope icon-lg icon-fw"></span>
                                <span class="badge badge-danger pull-right">8</span>
                                الرسائل
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <div class="dropdown-header">
                                <a class="dropdown-link" href="compose.html">رسالة جديدة</a>
                                <h5 class="dropdown-heading">الرسائل الأخيرة</h5>
                            </div>
                            <div class="dropdown-body">
                                <div class="list-group list-group-divided custom-scrollbar">
                                    <a class="list-group-item" href="#">
                                        <div class="notification">
                                            <div class="notification-media">
                                                <img class="rounded" width="40" height="40" src="{{ asset('assets/admin-theme') }}/images/user.jpg"
                                                    alt="Harry Jones">
                                            </div>
                                            <div class="notification-content">
                                                <small class="notification-timestamp">16 ساعة</small>
                                                <h5 class="notification-heading">أحمد</h5>
                                                <p class="notification-text">
                                                    <small class="truncate">
                                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="dropdown-footer">
                                <a class="dropdown-btn" href="#">عرض الكل</a>
                            </div>
                        </div>
                    </li>

                    <!-- notifications -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            aria-haspopup="true">
                            <span class="icon-with-child hidden-xs">
                                <span class="icon icon-bell-o icon-lg"></span>
                                <span class="badge badge-danger badge-above right">7</span>
                            </span>
                            <span class="visible-xs-block">
                                <span class="icon icon-bell icon-lg icon-fw"></span>
                                <span class="badge badge-danger pull-right">7</span>
                                التنبيهات
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                            <div class="dropdown-header">
                                <!-- <a class="dropdown-link" href="#">Mark all as read</a> -->
                                <h5 class="dropdown-heading">التنبيهات الأخيرة</h5>
                            </div>
                            <div class="dropdown-body">
                                <div class="list-group list-group-divided custom-scrollbar">
                                    <a class="list-group-item" href="#">
                                        <div class="notification">
                                            <div class="notification-media">
                                                <span
                                                    class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>
                                            </div>
                                            <div class="notification-content">
                                                <small class="notification-timestamp">35 دقيقة</small>
                                                <h5 class="notification-heading">تعديل بيانات</h5>
                                                <p class="notification-text">
                                                    <small class="truncate">
                                                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="dropdown-footer">
                                <a class="dropdown-btn" href="#">مشاهدة الكل</a>
                            </div>
                        </div>
                    </li>

                    <!-- profile link -->
                    <li class="dropdown hidden-xs">
                        <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                            <img class="rounded" width="36" height="36" src="{{ asset('assets/admin-theme') }}/images/user.jpg" alt="الاسناد الرقمي">
                            المدير
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="upgrade.html">
                                    <h5 class="navbar-upgrade-heading">
                                        البروفايل
                                        <small class="navbar-upgrade-notification">
                                        هذا النص هو مثال لنص يمكن أن يستبدل
                                        </small>
                                    </h5>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="navbar-upgrade-version">القوائم</li>
                            <li class="divider"></li>
                            <li><a href="#">التحكم</a></li>
                            <li><a href="#">تعديل الرمز السري</a></li>
                            <li><a href="#">تسجيل الخروج</a></li>
                        </ul>
                    </li>

                    <!-- قوائم الموبايل -->
                    <li class="visible-xs-block">
                        <a href="contacts.html">
                            <span class="icon icon-users icon-lg icon-fw"></span>
                            التحكم للموبايل
                        </a>
                    </li>
                    <li class="visible-xs-block">
                        <a href="profile.html">
                            <span class="icon icon-user icon-lg icon-fw"></span>
                            تعديل للموبايل
                        </a>
                    </li>
                    <li class="visible-xs-block">
                        <a href="login-1.html">
                            <span class="icon icon-power-off icon-lg icon-fw"></span>
                            تسجيل الخروج للموبايل
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>