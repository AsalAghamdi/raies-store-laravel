<script src="{{ asset('assets/admin-theme') }}/js/vendor.min.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/elephant.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/application.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/demo.min.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/profile.min.js"></script>
<script src="{{ asset('assets/admin-theme') }}/js/compose.min.js"></script>

<!-- Islamic Calendar | DatePicker -->
<script src="{{ asset('assets/admin-theme') }}/js/moment.js"></script>
<link href="{{ asset('assets/admin-theme') }}/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<script src="{{ asset('assets/admin-theme') }}/js/bootstrap-hijri-datetimepicker.min.js"></script>