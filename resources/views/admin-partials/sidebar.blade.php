<div class="layout-sidebar">
    <div class="layout-sidebar-backdrop"></div>
    <div class="layout-sidebar-body">
        <div class="custom-scrollbar">
            <nav id="sidenav" class="sidenav-collapse collapse">
                <ul class="sidenav">
                    <li class="sidenav-heading">المنتجات</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-products')}}">
                            <span class="sidenav-icon icon icon-diamond"></span>
                            <span class="sidenav-label">عرض جميع المنتجات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-create-product')}}">
                            <span class="sidenav-icon icon icon-plus-circle"></span>
                            <span class="sidenav-label">اضافة منتج</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-measurements')}}">
                            <span class="sidenav-icon icon icon-question-circle"></span>
                            <span class="sidenav-label">تحديد الاصناف والمقاسات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-gift')}}">
                            <span class="sidenav-icon icon icon-gift"></span>
                            <span class="sidenav-label">الاهداء</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">الطلبات</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-orders')}}">
                            <span class="sidenav-icon icon icon-th-list"></span>
                            <span class="sidenav-label">عرض جميع الطلبات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('special-orders')}}">
                            <span class="sidenav-badge badge badge-danger">26</span>
                            <span class="sidenav-icon icon icon-archive"></span>
                            <span class="sidenav-label">الطلبات الخاصة</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('return-orders')}}">
                            <span class="sidenav-icon icon icon-exchange"></span>
                            <span class="sidenav-label">الاسترجاع</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">الخصومات والعروض</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-coupons')}}">
                            <span class="sidenav-icon icon icon-ticket"></span>
                            <span class="sidenav-label">الكوبونات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-discount')}}">
                            <span class="sidenav-icon icon icon-percent"></span>
                            <span class="sidenav-label">عروض الخصومات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('banner-offers')}}">
                            <span class="sidenav-icon icon icon-television"></span>
                            <span class="sidenav-label">المحتوى الدعائي بالموقع</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('special-coupons')}}">
                            <span class="sidenav-icon icon icon-shopping-bag"></span>
                            <span class="sidenav-label">القسائم الشرائية الخاصة</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-bitcoin"></span>
                            <span class="sidenav-label">نظام النقاط</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">العملاء</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-customers')}}">
                            <span class="sidenav-icon icon icon-users"></span>
                            <span class="sidenav-label">عرض جميع العملاء</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('customer-category')}}">
                            <span class="sidenav-icon icon icon-certificate"></span>
                            <span class="sidenav-label">عضويات العملاء</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">شركة الشحن SMSA</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-truck"></span>
                            <span class="sidenav-label">حالة اشتراك الخدمة </span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('tracking')}}">
                            <span class="sidenav-icon icon icon-map-marker"></span>
                            <span class="sidenav-label">نظام تتبع الطلب</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">طرق الدفع</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-credit-card-alt"></span>
                            <span class="sidenav-label">خدمات الدفع المفعلة</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-plus-square"></span>
                            <span class="sidenav-label">اضافة خدمة دفع جديدة</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">تحليلات الموقع</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-bar-chart"></span>
                            <span class="sidenav-label">لوحة المعلومات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-table"></span>
                            <span class="sidenav-label">التقارير</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">محتوى الموقع</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-television"></span>
                            <span class="sidenav-label">قالب الموقع</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-language"></span>
                            <span class="sidenav-label">اللغة والعملة</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-comments-o"></span>
                            <span class="sidenav-label">المجتمع</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-envelope"></span>
                            <span class="sidenav-label">معلومات التواصل</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <li class="sidenav-heading">حسابات الادارة</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-list-ul"></span>
                            <span class="sidenav-label">عرض جميع الحسابات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-wpforms"></span>
                            <span class="sidenav-label">انشاء حساب جديد</span>
                        </a>
                    </li>
                    <!-- ---------------------------------- -->
                    <!-- <li class="sidenav-heading">الرئيسية</li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-profile')}}">
                            <span class="sidenav-icon icon icon-user"></span>
                            <span class="sidenav-label">البروفايل</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-chart')}}">
                            <span class="sidenav-icon icon icon-pie-chart"></span>
                            <span class="sidenav-label">Charts</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-tables')}}" aria-haspopup="true">
                            <span class="sidenav-icon icon icon-list"></span>
                            <span class="sidenav-label">Tables</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-datatables')}}" aria-haspopup="true">
                            <span class="sidenav-icon icon icon-list"></span>
                            <span class="sidenav-label">DataTables</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-classes')}}">
                            <span class="sidenav-label">الكلاسات</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-forms')}}">
                            <span class="sidenav-icon icon icon-edit"></span>
                            <span class="sidenav-label">الفورم</span>
                        </a>
                    </li>
                    <li class="sidenav-item">
                        <a href="{{url('admin-steps-form')}}">
                            <span class="sidenav-icon icon icon-edit"></span>
                            <span class="sidenav-label">فورم في خطوات</span>
                        </a>
                    </li> -->
                </ul>
            </nav>
        </div>
    </div>
</div>