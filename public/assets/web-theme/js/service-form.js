const form = document.querySelector('#event-registration');
const requiredRequest = document.querySelectorAll('.required-request'); // all required input field
const requiredRules = document.querySelectorAll('.required-rules'); // all required input field
const nextBtn = document.querySelectorAll('.nextBtn');
const infoForm = document.querySelector('.info-form'),
    requestForm = document.querySelector('.request-form'),
    rulesForm = document.querySelector('.rules-form'),
    srvcOrdrIcns = document.querySelector('#service-order-icons').childNodes; //this will get each li in the (service-order-icons) ul 
srvcOrdrIcns[1].addEventListener('click', function () {
    if (form.checkValidity() === false) {
        form.classList.add('was-validated');
    } else {
        infoForm.style.display = 'flex';
        requestForm.style.display = 'none';
        rulesForm.style.display = 'none';
        srvcOrdrIcns[1].classList.add('active');
        srvcOrdrIcns[3].classList.remove('active');
        srvcOrdrIcns[5].classList.remove('active');
    }
});
srvcOrdrIcns[3].addEventListener('click', function () {
    if (form.checkValidity() === false) {
        form.classList.add('was-validated');
    } else {
        infoForm.style.display = 'none';
        requestForm.style.display = 'flex';
        rulesForm.style.display = 'none';
        srvcOrdrIcns[1].classList.remove('active');
        srvcOrdrIcns[3].classList.add('active');
        srvcOrdrIcns[5].classList.remove('active');
        form.classList.remove('was-validated');
        requiredRequest.forEach(function (node) {
            node.required = true;
        });
    }
});
srvcOrdrIcns[5].addEventListener('click', function () {
    if (form.checkValidity() === false) {
        form.classList.add('was-validated');
    } else {
        infoForm.style.display = 'none';
        requestForm.style.display = 'none';
        rulesForm.style.display = 'flex';
        srvcOrdrIcns[1].classList.remove('active');
        srvcOrdrIcns[3].classList.remove('active');
        srvcOrdrIcns[5].classList.add('active');
        form.classList.remove('was-validated');
        requiredRules.forEach(function (node) {
            node.required = true;
        });
    }
});
nextBtn.forEach(function (btn) {
    btn.addEventListener('click', function () {
        if (infoForm.style.display != 'none') {
            if (form.checkValidity() === false) {
                form.classList.add('was-validated');
            } else {
                infoForm.style.display = 'none';
                requestForm.style.display = 'flex';
                location.href = "#service-order";
                srvcOrdrIcns[1].classList.remove('active');
                srvcOrdrIcns[3].classList.add('active');
                form.classList.remove('was-validated');
                requiredRequest.forEach(function (node) {
                    node.required = true;
                });
            }
        } else if (requestForm.style.display != 'none') {
            if (form.checkValidity() === false) {
                form.classList.add('was-validated');
            } else {
                requestForm.style.display = 'none';
                rulesForm.style.display = 'flex';
                location.href = "#service-order";
                srvcOrdrIcns[3].classList.remove('active');
                srvcOrdrIcns[5].classList.add('active');
                requiredRules.forEach(function (node) {
                    node.required = true;
                });
            }
        }
    });
});

/* visual production from */
function requiredRequestForVisualProduction(sec){
    if (sec === 'visual') {
        const requiredRequest1 = document.querySelectorAll('.required-request-visual'); // all required input field
        requiredRequest1.forEach(function (node) {
            node.required = true;
        });
        const requiredRequest2 = document.querySelectorAll('.required-request-design'); // all required input field
        requiredRequest2.forEach(function (node) {
            node.required = false;
        });
        const requiredRequest3 = document.querySelectorAll('.required-request-video'); // all required input field
        requiredRequest3.forEach(function (node) {
            node.required = false;
        });
    }
    if (sec === 'design') {
        const requiredRequest1 = document.querySelectorAll('.required-request-visual'); // all required input field
        requiredRequest1.forEach(function (node) {
            node.required = false;
        });
        const requiredRequest2 = document.querySelectorAll('.required-request-design'); // all required input field
        requiredRequest2.forEach(function (node) {
            node.required = true;
        });
        const requiredRequest3 = document.querySelectorAll('.required-request-video'); // all required input field
        requiredRequest3.forEach(function (node) {
            node.required = false;
        });
    }
    if (sec === 'video') {
        const requiredRequest1 = document.querySelectorAll('.required-request-visual'); // all required input field
        requiredRequest1.forEach(function (node) {
            node.required = false;
        });
        const requiredRequest2 = document.querySelectorAll('.required-request-design'); // all required input field
        requiredRequest2.forEach(function (node) {
            node.required = false;
        });
        const requiredRequest3 = document.querySelectorAll('.required-request-video'); // all required input field
        requiredRequest3.forEach(function (node) {
            node.required = true;
        });
    }
}

hideFormElm = (selector) => {
    console.log("yes");
    
    document.querySelectorAll(`${selector} .required`).forEach((elm) => {

        console.log("elm: " + elm.tagName);
        elm.classList.toggle('required');
        console.log("elm: " + elm.classList);
        elm.classList.toggle('not-required');
    });
}